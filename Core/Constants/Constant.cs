﻿namespace Domain.Constants
{
    public static class Constant
    {
        // Exception
        public const string UserNotFound = "User not found!";
        public const string BookNotFound = "Book not found!";
        public const string EmailNotFound = "Email not found!";
        public const string GenreInvalid = "Genre is invalid!";
        public const string EmailExists = "Email already exists!";
        public const string MoreThanOneAdmin = "Cannot have more than one admin!";
        public const string CannotHigherThanStock = "Quantity cannot be higher than book stock quantity!";
        public const string CannotHigherThanCart = "Quantity cannot be higher than cart item quantity!";
        public const string NotEnoughMoney = "Not enough money!";
        public const string LowerThanZero = "Cannot be lower than zero!";
        public const string LowerThanOne = "Cannot be lower than one!";
        public const string MustBetweenOneAndFive = "Rating must be between 1 and 5";

        // Announcement
        public const string DeleteSuccess = "Delete successfully!";
        public const string UpdateSuccess = "Update successfully!";
        public const string RegisterSuccess = "Register successfully!";
        public const string ResetSuccess = "Reset password successfully!";
        public const string ChargeMoneySuccess = "Charge money successfully!";
        public const string CheckoutSuccess = "Checkout successfully!";
        public const string AddReviewSuccess = "Add review successfully";
        public const string AddFavoriteSuccess = "Add favorite book successfully";
        public const string SendReportSuccess = "Send report successfully!";
        public const string OTPSent = "OTP is sent to your email!";
    }
}

﻿namespace Domain.Commons
{
    public class Pagination<T>
    {
        public int TotalItemCount { get; set; }

        public int PageSize { get; set; }

        public int TotalPageNumbers
        {
            get
            {
                var temp = TotalItemCount / PageSize;

                if (TotalItemCount % PageSize == 0)
                {
                    return temp;
                }

                return temp + 1;
            }
        }

        public int PageNumber { get; set; }

        public bool Next => PageNumber + 1 < TotalPageNumbers;

        public bool Previous => PageNumber > 0;

        public ICollection<T>? Items { get; set; }
    }
}

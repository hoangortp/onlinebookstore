﻿namespace Domain.Commons
{
    public class JWTSection
    {
        public string? SecretKey { get; set; }

        public string? Issuer { get; set; }

        public string? Audience { get; set; }

        public int ExpiresInMinutes { get; set; }
    }

    public class EmailSection
    {
        public string? Email { get; set; }

        public string? AppPassword { get; set; }

        public string? ReportSubject { get; set; }

        public string? ReportBody { get; set; }
    }

    public class AppConfiguration
    {
        public string? DatabaseConnection { get; set; }

        public JWTSection? JWTSection { get; set; }

        public EmailSection? EmailSection { get; set; }
    }
}

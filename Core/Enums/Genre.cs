﻿namespace Domain.Enums
{
    public enum Genre
    {
        Fantasy,
        Crime,
        Action,
        Adventure,
        Drama,
        Horror,
        Romance,
        Comedy,
        SciFi // Science fiction
    }
}

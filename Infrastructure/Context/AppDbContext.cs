﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Infrastructure.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<OTP> OTPs { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<FavoriteBook> FavoriteBooks { get; set; }
        public DbSet<CartItem> CartItems { get; set; }
        public DbSet<Review> Reviews { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            modelBuilder.Entity<User>()
                        .HasMany(u => u.Orders)
                        .WithOne(o => o.User)
                        .HasForeignKey(o => o.UserId);

            modelBuilder.Entity<User>()
                        .HasMany(u => u.CartItems)
                        .WithOne(ci => ci.User)
                        .HasForeignKey(ci => ci.UserId);

            modelBuilder.Entity<User>()
                        .HasMany(u => u.FavoriteBooks)
                        .WithOne(fb => fb.User)
                        .HasForeignKey(fb => fb.UserId);

            modelBuilder.Entity<Book>()
                        .HasMany(b => b.OrderDetails)
                        .WithOne(od => od.Book)
                        .HasForeignKey(od => od.BookId);

            modelBuilder.Entity<Order>()
                        .HasMany(o => o.OrderDetails)
                        .WithOne(od => od.Order)
                        .HasForeignKey(od => od.OrderId);
        }
    }
}

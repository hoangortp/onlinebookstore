﻿using Application.Interfaces;
using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Context;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class FavoriteBookRepository : GenericRepository<FavoriteBook>, IFavoriteBookRepository
    {
        public FavoriteBookRepository(AppDbContext context, ICurrentTimeService currentTimeService) : base(context, currentTimeService)
        {
        }

        public async Task<FavoriteBook> GetFavoriteBookByUserIdAndBookId(Guid userId, Guid bookId)
        {
            var favoriteBook = await Context.FavoriteBooks.FirstOrDefaultAsync(x => x.UserId == userId 
                                                                                     && x.BookId == bookId);

            return favoriteBook!;
        }
    }
}

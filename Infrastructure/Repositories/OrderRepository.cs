﻿using Application.Interfaces;
using Domain.Commons;
using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Context;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        public OrderRepository(AppDbContext context, ICurrentTimeService currentTimeService) : base(context, currentTimeService)
        {
        }

        public async Task<Pagination<Order>> Paging(Guid userId, int pageNumber, int pageSize)
        {
            var itemCount = await Context.Orders.CountAsync();
            var items = await Context.Orders.Where(order => order.UserId == userId)
                                             .OrderByDescending(order => order.CreatedAt)
                                             .Skip(pageNumber * pageSize)
                                             .Take(pageSize)
                                             .AsNoTracking()
                                             .ToListAsync();

            var result = new Pagination<Order>
            {
                PageNumber = pageNumber,
                PageSize = pageSize,
                TotalItemCount = itemCount,
                Items = items
            };

            return result;
        }

        public async Task<int> CountTodayOrder()
        {
            DateTime today = DateTime.Today;
            DateTime tomorrow = today.AddDays(1);

            int orderCount = await Context.Orders.Where(order => order.CreatedAt >= today && order.CreatedAt < tomorrow).CountAsync();

            return orderCount;
        }

        public async Task<int> CountTodayPaidOrder()
        {
            int paidOrderCount = await Context.Orders.Where(order => order.PaidDate > DateTime.MinValue).CountAsync();

            return paidOrderCount;
        }

        public async Task<decimal> GetTotalIncome()
        {
            decimal income = await Context.Orders.SumAsync(order => order.TotalPrice);

            return income;
        }
    }
}

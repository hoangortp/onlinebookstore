﻿using Application.Interfaces;
using Domain.Entities;
using Infrastructure.Context;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using System.Reflection;
using Domain.Commons;
using Domain.Repositories;

namespace Infrastructure.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        protected readonly AppDbContext Context;
        private readonly ICurrentTimeService _currentTimeService;
        private readonly DbSet<T> _dbSet;

        public GenericRepository(AppDbContext context, ICurrentTimeService currentTimeService)
        {
            Context = context;
            _currentTimeService = currentTimeService;
            _dbSet = Context.Set<T>();
        }

        public async Task<ICollection<T>> GetAllAsync() => await _dbSet.ToListAsync();

        public T GetById(Guid id)
        {
            PropertyInfo idProperty = typeof(T).GetProperty("Id")!;

            return _dbSet.AsEnumerable().FirstOrDefault(x => (Guid)idProperty!.GetValue(x)! == id)!;
        }

        public async Task AddAsync(T entity)
        {
            entity.CreatedAt = _currentTimeService.GetCurrentTime();
            await _dbSet.AddAsync(entity);
        }

        public async Task AddRangeAsync(ICollection<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.CreatedAt = _currentTimeService.GetCurrentTime();
            }

            await _dbSet.AddRangeAsync(entities);
        }

        public void Update(T entity)
        {
            entity.UpdatedAt = _currentTimeService.GetCurrentTime();
            _dbSet.Update(entity);
        }

        public void UpdateRange(ICollection<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.UpdatedAt = _currentTimeService.GetCurrentTime();
            }

            _dbSet.UpdateRange(entities);
        }

        public void SoftDelete(T entity)
        {
            entity.IsDeleted = true;
            entity.DeletedAt = _currentTimeService.GetCurrentTime();
            Update(entity);
        }

        public void SoftDeleteRange(ICollection<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.IsDeleted = true;
                entity.DeletedAt = _currentTimeService.GetCurrentTime();
            }
        }

        public void HardDelete(T entity) => _dbSet.Remove(entity);

        public void HardDeleteRange(ICollection<T> entities) => _dbSet.RemoveRange(entities);

        public async Task<Pagination<T>> Paging(int pageNumber, int pageSize)
        {
            var itemCount = await _dbSet.CountAsync();
            var items = await _dbSet.OrderBy(x => x.CreatedAt)
                                    .Skip(pageNumber * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<T>()
            {
                PageNumber = pageNumber,
                PageSize = pageSize,
                TotalItemCount = itemCount,
                Items = items
            };

            return result;
        }

        public async Task<Pagination<T>> Filter(Expression<Func<T, bool>> predicate, int pageNumber, int pageSize)
        {
            var filter = await _dbSet.Where(predicate).ToListAsync();
            var itemCount = filter.Where(x => x.IsDeleted == false).Count();
            var items = filter.Where(x => x.IsDeleted == false)
                                    .OrderBy(x => x.CreatedAt)
                                    .Skip(pageNumber * pageSize)
                                    .Take(pageSize)
                                    .ToList();

            var result = new Pagination<T>()
            {
                PageNumber = pageNumber,
                PageSize = pageSize,
                TotalItemCount = itemCount,
                Items = items
            };

            return result;
        }
    }
}

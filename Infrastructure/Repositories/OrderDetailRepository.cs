﻿using Application.Interfaces;
using Domain.Commons;
using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Context;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class OrderDetailRepository : GenericRepository<OrderDetail>, IOrderDetailRepository
    {
        public OrderDetailRepository(AppDbContext context, ICurrentTimeService currentTimeService) : base(context, currentTimeService)
        {
        }

        public async Task<Pagination<OrderDetail>> Paging(Guid orderId, int pageNumber, int pageSize)
        {
            var itemCount = await Context.OrderDetails.CountAsync();
            var items = await Context.OrderDetails.Where(x => x.OrderId == orderId)
                                                   .OrderByDescending(x => x.CreatedAt)
                                                   .Skip(pageNumber * pageSize)
                                                   .Take(pageSize)
                                                   .AsNoTracking()
                                                   .ToListAsync();

            var result = new Pagination<OrderDetail>
            {
                PageNumber = pageNumber,
                PageSize = pageSize,
                TotalItemCount = itemCount,
                Items = items
            };

            return result;
        }
    }
}

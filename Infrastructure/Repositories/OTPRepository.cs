﻿using Application.Interfaces;
using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Context;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class OTPRepository : GenericRepository<OTP>, IOTPRepository
    {
        public OTPRepository(AppDbContext context, ICurrentTimeService currentTimeService) : base(context, currentTimeService)
        {
        }

        public async Task<OTP> GetOTPByEmailAndOTPCode(string email, int otpCode)
        {
            return await Context!.OTPs!.FirstOrDefaultAsync(x => x!.Email == email && x!.OTPCode == otpCode)!;
        }
    }
}

﻿using Application.Interfaces;
using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Context;

namespace Infrastructure.Repositories
{
    public class BookRepository : GenericRepository<Book>, IBookRepository
    {
        public BookRepository(AppDbContext context, ICurrentTimeService currentTimeService) : base(context, currentTimeService)
        {
        }

        public string GetBookTitleById(Guid bookId)
        {
            var book = Context.Books.FirstOrDefault(x => x.Id == bookId);

            return book?.Title;
        }

        public Guid GetIdByBookTitle(string bookTitle)
        {
            var book = Context.Books.FirstOrDefault(x => x.Title == bookTitle);

            return book.Id;
        }
    }
}

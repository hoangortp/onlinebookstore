﻿using Application.Interfaces;
using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Context;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class CartItemRepository : GenericRepository<CartItem>, ICartItemRepository
    {
        public CartItemRepository(AppDbContext context, ICurrentTimeService currentTimeService) : base(context, currentTimeService)
        {
        }

        public async Task<CartItem> GetCartItemByUserIdAndBookId(Guid userId, Guid bookId)
        {
            var cartItem = await Context.CartItems.FirstOrDefaultAsync(x => x.UserId == userId 
                                                                             && x.BookId == bookId 
                                                                             && x.IsDeleted == false);

            return cartItem!;
        }

        public List<CartItem> ViewCartItems(Guid userId)
        {
            return Context.CartItems.Include(x => x.User)
                                     .Include(x => x.Book)
                                     .Where(x => x.UserId == userId)
                                     .ToList();
        }
    }
}

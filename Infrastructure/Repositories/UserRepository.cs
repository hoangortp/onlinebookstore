﻿using Application.Interfaces;
using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Context;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(AppDbContext context, ICurrentTimeService currentTimeService) : base(context, currentTimeService)
        {
        }

        public Task<bool> CheckAdminExists() => Context.Users.AnyAsync(user => user.Role == "admin");

        public Task<bool> CheckEmailExisted(string email) => Context.Users.AnyAsync(user => user.Email == email);

        public async Task<User> GetEmailAndHashedPassword(string email, string hashedPassword)
        {
            var user = await Context.Users.FirstOrDefaultAsync(user => user.Email == email && user.HashedPassword == hashedPassword) 
                             ?? throw new Exception("Email or password is invalid");

            return user;
        }

        public async Task<string> GetRoleBasedOnEmail(string email)
        {
            var userRole = await Context.Users.Where(user => user.Email == email)
                                               .Select(user => user.Role)
                                               .FirstOrDefaultAsync() 
                                 ?? throw new Exception("User not found");

            return userRole;
        }

        public async Task<User> GetUserBasedOnEmail(string email)
        {
            var user = await Context.Users.FirstOrDefaultAsync(user => user.Email == email) 
                       ?? throw new Exception("Email or password is invalid");

            return user;
        }

        public async Task<int> CountNewAccount()
        {
            DateTime today = DateTime.Today;
            DateTime tomorrow = today.AddDays(1);

            var newAccountCount = await Context.Users.Where(user => user.CreatedAt >= today && user.CreatedAt < tomorrow).CountAsync();

            return newAccountCount;
        }
    }
}

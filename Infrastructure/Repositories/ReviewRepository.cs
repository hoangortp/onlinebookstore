﻿using Application.Interfaces;
using Domain.Commons;
using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Context;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class ReviewRepository : GenericRepository<Review>, IReviewRepository
    {
        public ReviewRepository(AppDbContext context, ICurrentTimeService currentTimeService) : base(context, currentTimeService)
        {
        }

        public async Task<Pagination<Review>> Paging(Guid bookId, int pageNumber, int pageSize)
        {
            var itemCount = await Context.Reviews.CountAsync();
            var items = await Context.Reviews.Where(x => x.BookId == bookId)
                                          .OrderByDescending(x => x.CreatedAt)
                                          .Skip(pageNumber * pageSize)
                                          .Take(pageSize)
                                          .AsNoTracking()
                                          .ToListAsync();

            Pagination<Review> result = new Pagination<Review>
            {
                TotalItemCount = itemCount,
                Items = items,
                PageNumber = pageNumber,
                PageSize = pageSize
            };

            return result;
        }
    }
}

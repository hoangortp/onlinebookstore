﻿using Application;
using Domain.Repositories;
using Infrastructure.Context;

namespace Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        private readonly IBookRepository _bookRepository;
        private readonly IUserRepository _userRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderDetailRepository _orderDetailRepository;
        private readonly IReviewRepository _reviewRepository;
        private readonly ICartItemRepository _cartItemRepository;
        private readonly IFavoriteBookRepository _favoriteBookRepository;
        private readonly IOTPRepository _otpRepository;

        public UnitOfWork(AppDbContext context,
                          IBookRepository bookRepository,
                          IUserRepository userRepository,
                          IOrderRepository orderRepository,
                          IOrderDetailRepository orderDetailRepository,
                          IReviewRepository reviewRepository,
                          ICartItemRepository cartItemRepository,
                          IFavoriteBookRepository favoriteBookRepository,
                          IOTPRepository otpRepository)
        {
            _context = context;
            _bookRepository = bookRepository;
            _userRepository = userRepository;
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
            _reviewRepository = reviewRepository;
            _cartItemRepository = cartItemRepository;
            _favoriteBookRepository = favoriteBookRepository;
            _otpRepository = otpRepository;
        }

        public IBookRepository BookRepository => _bookRepository;

        public IUserRepository UserRepository => _userRepository;

        public IOrderRepository OrderRepository => _orderRepository;

        public IOrderDetailRepository OrderDetailRepository => _orderDetailRepository;

        public IReviewRepository ReviewRepository => _reviewRepository;

        public ICartItemRepository CartItemRepository => _cartItemRepository;

        public IFavoriteBookRepository FavoriteBookRepository => _favoriteBookRepository;

        public IOTPRepository OTPRepository => _otpRepository;

        public async Task<int> SaveChangeAsync() => await _context.SaveChangesAsync();
    }
}

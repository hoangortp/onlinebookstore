﻿using Application.DTOModel;
using Application.DTOModel.DTOBookModels;
using Application.ViewModels;
using AutoMapper;
using Domain.Commons;
using Domain.Entities;
using Presentation.WebAPI.ViewModels;

namespace Infrastructure.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap(typeof(Pagination<>), typeof(Pagination<>));

            // To DTO
            CreateMap<DTOBook, Book>().ReverseMap();
            CreateMap<DTOCartItem, CartItem>().ReverseMap();
            CreateMap<DTOOrder, Order>().ReverseMap();
            CreateMap<DTOOrderDetail, OrderDetail>().ReverseMap();
            
            // To ViewModel
            CreateMap<DTOBook, BookVM>().ReverseMap();
            CreateMap<DTOOrder, OrderVM>().ReverseMap();
            CreateMap<DTOOrderDetail, OrderDetailVM>().ReverseMap();
            CreateMap<Review, ReviewVM>().ReverseMap();
        }
    }
}

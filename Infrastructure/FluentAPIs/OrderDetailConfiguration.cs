﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.FluentAPIs
{
    public class OrderDetailConfiguration : IEntityTypeConfiguration<OrderDetail>
    {
        public void Configure(EntityTypeBuilder<OrderDetail> builder)
        {
            builder.HasKey(x => new { x.OrderId, x.BookId });

            builder.Property(x => x.SubtotalPrice).HasColumnType("decimal(5,2)");

            builder.HasQueryFilter(x => x.IsDeleted == false);
        }
    }
}

using Infrastructure;
using Presentation.WebAPI;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Application;
using Presentation.WebAPI.Middlewares;
using Hangfire;
using Application.Interfaces;
using Domain.Commons;

var builder = WebApplication.CreateBuilder(args);

// Parse the configuration from appsettings.json
var configuration = builder.Configuration.Get<AppConfiguration>();
builder.Services.AddInfrastructureService(configuration.DatabaseConnection);
builder.Services.AddApplicationServices();
builder.Services.AddWebAPIService();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(option =>
                {
                    option.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = configuration.JWTSection?.Issuer,
                        ValidAudience = configuration.JWTSection?.Audience,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration.JWTSection?.SecretKey))
                    };
                });

builder.Services.AddSingleton(configuration);

var app = builder.Build();

app.UseMiddleware<GlobalExceptionMiddleware>();

app.UseHealthChecks("/healthcheck");

app.UseStaticFiles();

app.UseHangfireDashboard();
RecurringJob.AddOrUpdate<IDailyReportService>("Send daily report to admin", x => x.GenerateDailyReport(), "*/59 23 * * *");

app.UseAuthorization();

app.MapControllers();

app.Run();

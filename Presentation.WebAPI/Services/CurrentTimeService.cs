﻿using Application.Interfaces;

namespace Presentation.WebAPI.Services
{
    public class CurrentTimeService : ICurrentTimeService
    {
        public DateTime GetCurrentTime() => DateTime.Now;
    }
}

﻿using Application.Interfaces;
using Hangfire;
using Presentation.WebAPI.Middlewares;
using Presentation.WebAPI.Services;

namespace Presentation.WebAPI
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddWebAPIService(this IServiceCollection services)
        {
            services.AddScoped<ICurrentTimeService, CurrentTimeService>();
            services.AddSingleton<GlobalExceptionMiddleware>();

            services.AddControllers();
            services.AddHealthChecks();
            services.AddHangfireServer();
            
            return services;
        }
    }
}

﻿using Application.DTOModel;
using Application.Interfaces;
using Application.Interfaces.IuserServices;
using Application.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Presentation.WebAPI.Controllers.WebAPI.Controllers;
using System.Security.Claims;
using Domain.Commons;

namespace Presentation.WebAPI.Controllers
{
    [Route("order-details")]
    public class OrderDetailController : BaseController
    {
        private readonly IOrderDetailService _orderDetailService;
        private readonly IUserService _userService;

        public OrderDetailController(IOrderDetailService orderDetailService, IUserService userService)
        {
            _orderDetailService = orderDetailService;
            _userService = userService;
        }

        [HttpGet("get")]
        [Authorize(Roles = "customer")]
        public async Task<Pagination<OrderDetailVM>> ViewOrderDetails(DTOOrderId orderId, int pageNumber, int pageSize)
        {
            var userEmail = HttpContext.User.FindFirst(ClaimTypes.Email)!.Value;

            await _userService.GetUserByEmail(userEmail);

            return await _orderDetailService.ViewOrderDetails(orderId.Id, pageNumber, pageSize);
        }
    }
}

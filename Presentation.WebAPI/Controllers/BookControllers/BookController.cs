﻿using Application.DTOModel.DTOBook;
using Application.DTOModel.DTOBookModels;
using Application.Interfaces.IBookServices;
using Application.Utils.Extensions;
using AutoMapper;
using Domain.Commons;
using Domain.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Presentation.WebAPI.Controllers.WebAPI.Controllers;
using Presentation.WebAPI.ViewModels;

namespace Presentation.WebAPI.Controllers.BookControllers
{
    [Route("books")]
    public class BookController : BaseController
    {
        private readonly IBookService _bookService;
        private readonly IMapper _mapper;

        public BookController(IBookService bookService, IMapper mapper)
        {
            _bookService = bookService;
            _mapper = mapper;
        }

        [HttpGet("view-book")]
        [Authorize(Roles = "customer,admin")]
        public BookVM ViewBookById(DTOBookId dtoBookId)
        {
            var dtoBook = _bookService.GetBookById(dtoBookId.Id);

            return dtoBook.ToCustomerBookViewModel();
        }

        [HttpGet("view-books")]
        [Authorize(Roles = "customer,admin")]
        public async Task<Pagination<BookVM>> ViewBooks(int pageNumber, int pageSize)
        {
            var dtoBooks = await _bookService.GetBooks(pageNumber, pageSize);
            var viewBooks = _mapper.Map<Pagination<BookVM>>(dtoBooks);

            return viewBooks;
        }

        [HttpPost("add-book")]
        [Authorize(Roles = "admin")]
        public async Task<DTOBook?> AddBook(DTOCreatedBook dtoCreateBook)
        {
            return await _bookService.AddBook(dtoCreateBook);
        }

        [HttpPost("add-books")]
        [Authorize(Roles = "admin")]
        public async Task<List<DTOBook>?> AddBooks(List<DTOCreatedBook> dtoCreateBooks)
        {
            return await _bookService.AddRangeBook(dtoCreateBooks);
        }

        [HttpPost("delete")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteBook(DTOBook dtoBook)
        {
            await _bookService.SoftDeleteBook(dtoBook.Id);

            return Ok(Constant.DeleteSuccess);
        }

        [HttpPost("delete-range")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteRangeBook(List<DTOBook> dtoBooks)
        {
            List<Guid> id = dtoBooks.Select(x => x.Id).ToList();

            await _bookService.SoftDeleteRangeBook(id);

            return Ok(Constant.DeleteSuccess);
        }

        [HttpPut("update")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateBook(DTOBook dtoBook)
        {
            await _bookService.UpdateBook(dtoBook);

            return Ok(Constant.UpdateSuccess);
        }
    }
}

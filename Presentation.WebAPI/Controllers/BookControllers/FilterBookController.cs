﻿using Application.DTOModel.DTOBookModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Presentation.WebAPI.Controllers.WebAPI.Controllers;
using AutoMapper;
using Presentation.WebAPI.ViewModels;
using Application.Interfaces.IBookServices;
using Domain.Commons;

namespace Presentation.WebAPI.Controllers.BookControllers
{
    [Route("books")]
    public class FilterBookController : BaseController
    {
        private readonly IFilterBookService _filterBookService;
        private readonly IMapper _mapper;

        public FilterBookController(IFilterBookService filterBookService, IMapper mapper)
        {
            _filterBookService = filterBookService;
            _mapper = mapper;
        }

        [Authorize(Roles = "customer, admin")]
        [HttpGet("filter-title")]
        public async Task<Pagination<BookVM>> FilterByTitle(DTOBook dtoBook, int pageNumber, int pageSize)
        {
            var dtoBooks = await _filterBookService.FilterByTitleAsync(dtoBook.Title, pageNumber, pageSize);
            var viewBooks = _mapper.Map<Pagination<BookVM>>(dtoBooks);

            return viewBooks;
        }

        [Authorize(Roles = "customer, admin")]
        [HttpGet("filter-author")]
        public async Task<Pagination<BookVM>> FilterByAuthor(DTOBook dtoBook, int pageNumber, int pageSize)
        {
            var dtoBooks = await _filterBookService.FilterByAuthorAsync(dtoBook.Author, pageNumber, pageSize);
            var viewBooks = _mapper.Map<Pagination<BookVM>>(dtoBooks);

            return viewBooks;
        }

        [Authorize(Roles = "customer, admin")]
        [HttpGet("filter-year")]
        public async Task<Pagination<BookVM>> FilterByYear(DTOBook dtoBook, int pageNumber, int pageSize)
        {
            var dtoBooks = await _filterBookService.FilterByYearAsync(dtoBook.PublicationYear, pageNumber, pageSize);
            var viewBooks = _mapper.Map<Pagination<BookVM>>(dtoBooks);

            return viewBooks;
        }

        [Authorize(Roles = "customer, admin")]
        [HttpGet("filter-genre")]
        public async Task<Pagination<BookVM>> FilterByGenre(DTOBook dtoBook, int pageNumber, int pageSize)
        {
            var dtoBooks = await _filterBookService.FilterByGenreAsync(dtoBook.Genre, pageNumber, pageSize);
            var viewBooks = _mapper.Map<Pagination<BookVM>>(dtoBooks);

            return viewBooks;
        }
    }
}

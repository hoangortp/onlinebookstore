﻿using Application.DTOModel;
using Application.Interfaces;
using Application.Interfaces.IuserServices;
using Application.ViewModels;
using AutoMapper;
using Domain.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Presentation.WebAPI.Controllers.WebAPI.Controllers;
using System.Security.Claims;
using Domain.Commons;

namespace Presentation.WebAPI.Controllers
{
    [Route("orders")]
    public class OrderController : BaseController
    {
        private readonly IOrderService _orderService;
        private readonly IOrderDetailService _orderDetailService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public OrderController(IOrderService orderService, 
                               IUserService userService, 
                               IMapper mapper, 
                               IOrderDetailService orderDetailService)
        {
            _orderService = orderService;
            _userService = userService;
            _mapper = mapper;
            _orderDetailService = orderDetailService;
        }

        [HttpPost("checkout")]
        [Authorize(Roles = "customer")]
        public async Task<IActionResult> CheckOut()
        {
            var userEmail = HttpContext.User.FindFirst(ClaimTypes.Email)!.Value;

            var user = await _userService.GetUserByEmail(userEmail);

            DTOOrder dtoOrder = new();

            var order = await _orderService.AddOrder(user.Id, dtoOrder);

            await _orderDetailService.AddOrderDetail(user.Id, order!);

            return Ok(Constant.CheckoutSuccess);
        }

        [HttpGet("view")]
        [Authorize(Roles = "customer")]
        public async Task<Pagination<OrderVM>> ViewOrders(int pageNumber, int pageSize)
        {
            var userEmail = HttpContext.User.FindFirst(ClaimTypes.Email)!.Value;

            var user = await _userService.GetUserByEmail(userEmail);

            var dtoOrders = await _orderService.ViewOrders(user.Id, pageNumber, pageSize);

            Pagination<OrderVM> ordersVM = _mapper.Map<Pagination<OrderVM>>(dtoOrders);

            return ordersVM;
        }
    }
}

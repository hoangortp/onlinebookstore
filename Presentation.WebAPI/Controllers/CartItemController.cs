﻿using Application.DTOModel;
using Application.Interfaces;
using Application.Interfaces.IuserServices;
using Application.ViewModels;
using Domain.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Presentation.WebAPI.Controllers.WebAPI.Controllers;
using System.Security.Claims;
using Domain.Commons;

namespace Presentation.WebAPI.Controllers
{
    [Route("cart")]
    public class CartItemController : BaseController
    {
        private readonly ICartItemService _cartItemService;
        private readonly IUserService _userService;

        public CartItemController(ICartItemService cartItemService, IUserService userService)
        {
            _cartItemService = cartItemService;
            _userService = userService;
        }

        [HttpPost("add")]
        [Authorize(Roles = "customer")]
        public async Task<DTOCartItem> AddToCart(DTOCartItem dtoCartItem)
        {
            var userEmail = HttpContext.User.FindFirst(ClaimTypes.Email)!.Value;

            var user = await _userService.GetUserByEmail(userEmail);

            dtoCartItem.UserId = user.Id;

            return await _cartItemService.AddToCart(dtoCartItem);
        }

        [HttpGet("view")]
        [Authorize(Roles = "customer")]
        public async Task<Pagination<CartItemVM>> ViewCartItem(int pageNumber, int pageSize)
        {
            var userEmail = HttpContext.User.FindFirst(ClaimTypes.Email)!.Value;

            var user = await _userService.GetUserByEmail(userEmail);

            return await _cartItemService.ViewCartItems(user.Id, pageNumber, pageSize);
        }

        [HttpPost("delete")]
        [Authorize(Roles = "customer")]
        public async Task<IActionResult> DeleteCartItem(DTOCartItem dtoCartItem)
        {
            var userEmail = HttpContext.User.FindFirst(ClaimTypes.Email)!.Value;

            var user = await _userService.GetUserByEmail(userEmail);

            dtoCartItem.UserId = user.Id;

            await _cartItemService.RemoveFromCart(dtoCartItem);

            return Ok(Constant.DeleteSuccess);
        }
    }
}

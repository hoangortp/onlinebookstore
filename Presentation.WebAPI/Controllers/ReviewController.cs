﻿using Application.DTOModel;
using Application.DTOModel.DTOBook;
using Application.Interfaces;
using Application.Interfaces.IuserServices;
using Application.ViewModels;
using Domain.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Presentation.WebAPI.Controllers.WebAPI.Controllers;
using System.Security.Claims;
using Domain.Commons;

namespace Presentation.WebAPI.Controllers
{
    [Route("reviews")]
    public class ReviewController : BaseController
    {
        private readonly IReviewService _reviewService;
        private readonly IUserService _userService;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ReviewController(IReviewService reviewService,
                                IUserService userService,
                                IWebHostEnvironment webHostEnvironment)
        {
            _reviewService = reviewService;
            _userService = userService;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpPost("add")]
        [Authorize(Roles = "customer")]
        public async Task<DTOReview> AddReview([FromForm] DTOReview dtoReview, [FromForm] IFormFile? image)
        {
            string imageFile = "images";

            var userEmail = HttpContext.User?.FindFirst(ClaimTypes.Email)?.Value;

            var user = await _userService.GetUserByEmail(userEmail!);

            dtoReview.UserId = user.Id;

            if (image is not null)
            {
                var webRootPath = _webHostEnvironment.WebRootPath;
                var uniqueFileName = $"{Guid.NewGuid()}{Path.GetExtension(image.FileName)}";
                string imagePath = Path.Combine(imageFile, uniqueFileName);
                var savePath = Path.Combine(webRootPath, imagePath);

                using (var stream = new FileStream(savePath, FileMode.Create))
                {
                    await image.CopyToAsync(stream);
                }

                var baseUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}";
                var imageUrl = $"{baseUrl}/Images/{uniqueFileName}";

                dtoReview.Image = imageUrl;
            }

            return await _reviewService.AddReview(dtoReview.UserId, dtoReview);
        }

        [HttpGet("view")]
        [Authorize(Roles = "customer")]
        public async Task<Pagination<ReviewVM>> ViewReviews(DTOBookId dtoBookId, int pageNumber, int pageSize)
        {
            var userEmail = HttpContext.User.FindFirst(ClaimTypes.Email)!.Value;

            var user = await _userService.GetUserByEmail(userEmail);

            return await _reviewService.ViewReviews(dtoBookId.Id, pageNumber, pageSize);
        }
    }
}

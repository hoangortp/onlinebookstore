﻿using Application.Interfaces;
using Domain.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Presentation.WebAPI.Controllers.WebAPI.Controllers;

namespace Presentation.WebAPI.Controllers
{
    [Route("report")]
    public class DailyReportController : BaseController
    {
        private readonly IDailyReportService _dailyReportService;

        public DailyReportController(IDailyReportService dailyReportService)
        {
            _dailyReportService = dailyReportService;
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> SendDailyReport()
        {
            await _dailyReportService.GenerateDailyReport();

            return Ok(Constant.SendReportSuccess);
        }
    }
}

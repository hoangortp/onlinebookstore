﻿using Application.DTOModel;
using Application.Interfaces;
using Application.Interfaces.IuserServices;
using Application.ViewModels;
using Domain.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Presentation.WebAPI.Controllers.WebAPI.Controllers;
using System.Security.Claims;
using Domain.Commons;

namespace Presentation.WebAPI.Controllers
{
    [Route("favorite")]
    public class FavoriteBookController : BaseController
    {
        private readonly IFavoriteBookService _favoriteBookService;
        private readonly IUserService _userService;

        public FavoriteBookController(IFavoriteBookService favoriteBookService, IUserService userService)
        {
            _favoriteBookService = favoriteBookService;
            _userService = userService;
        }

        [HttpPost("add")]
        [Authorize(Roles = "customer")]
        public async Task<DTOFavoriteBook> AddFavoriteBook(DTOFavoriteBook dtoFavoriteBook)
        {
            var userEmail = HttpContext.User.FindFirst(ClaimTypes.Email)!.Value;

            var user = await _userService.GetUserByEmail(userEmail);

            dtoFavoriteBook.UserId = user.Id;

            return await _favoriteBookService.AddFavoriteBook(dtoFavoriteBook);
        }

        [HttpGet("view")]
        [Authorize(Roles = "customer")]
        public async Task<Pagination<FavoriteBookVM>> ViewFavoriteBooks(int pageNumber, int pageSize)
        {
            var userEmail = HttpContext.User.FindFirst(ClaimTypes.Email)!.Value;

            var user = await _userService.GetUserByEmail(userEmail);

            return await _favoriteBookService.ViewFavoriteBooks(user.Id, pageNumber, pageSize);
        }

        [HttpPost("delete")]
        [Authorize(Roles = "customer")]
        public async Task<IActionResult> DeleteFavoriteBook(DTOFavoriteBook dtoFavoriteBook)
        {
            var userEmail = HttpContext.User.FindFirst(ClaimTypes.Email)!.Value;

            var user = await _userService.GetUserByEmail(userEmail);

            dtoFavoriteBook.UserId = user.Id;

            await _favoriteBookService.RemoveFavoriteBook(dtoFavoriteBook);

            return Ok(Constant.DeleteSuccess);
        }
    }
}

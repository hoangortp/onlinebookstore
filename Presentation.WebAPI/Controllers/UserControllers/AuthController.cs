﻿using Application.DTOModel.DTOUser;
using Application.DTOModel.DTOUserModels;
using Application.Interfaces.IuserServices;
using Domain.Constants;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Presentation.WebAPI.Controllers.WebAPI.Controllers;

namespace Presentation.WebAPI.Controllers.UserControllers
{
    [Route("auth")]
    public class AuthController : BaseController
    {
        private readonly IAuthService _authService;
        private readonly IMailService _mailService;

        public AuthController(IAuthService authService, IMailService mailService)
        {
            _authService = authService;
            _mailService = mailService;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(DTOUserRegister dtoUserRegister)
        {
            await _authService.RegisterAsync(dtoUserRegister);

            return Ok(Constant.RegisterSuccess);
        }

        [HttpPost("register-admin")]
        public async Task<IActionResult> RegisterAdmin(DTOAdminRegister dtoAdminRegister)
        {
            await _authService.RegisterAdminAsync(dtoAdminRegister);

            return Ok(Constant.RegisterSuccess);
        }

        [HttpPost("login")]
        public async Task<string> Login(DTOUserLogin dtoUserLogin)
        {
            return await _authService.LoginAsync(dtoUserLogin);
        }

        [HttpPost("forgot-password")]
        public async Task<IActionResult> ForgotPassword(User user)
        {
            await _mailService.SendOTPMailAsync(user.Email!);

            return Ok(Constant.OTPSent);
        }

        [HttpPatch("reset-password")]
        public async Task<IActionResult> ResetPassword(DTOUserResetPassword dtoUserResetPassword)
        {
            await _authService.ResetPassword(dtoUserResetPassword);

            return Ok(Constant.ResetSuccess);
        }
    }
}

﻿using Application.DTOModel.DTOUser;
using Application.Interfaces.IuserServices;
using Application.ViewModels;
using Domain.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Presentation.WebAPI.Controllers.WebAPI.Controllers;
using System.Security.Claims;

namespace Presentation.WebAPI.Controllers.UserControllers
{
    [Route("user")]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPut("update")]
        [Authorize(Roles = "customer,admin")]
        public async Task<IActionResult> UpdateUser(DTOUserUpdate dtoUserUpdate)
        {
            var userEmail = HttpContext.User.FindFirst(ClaimTypes.Email)!.Value;

            var user = await _userService.GetUserByEmail(userEmail);

            dtoUserUpdate.Id = user.Id;

            await _userService.UpdateUser(dtoUserUpdate);

            return Ok(Constant.UpdateSuccess);
        }


        [HttpPost("charge-money")]
        [Authorize(Roles = "customer")]
        public async Task<IActionResult> ChargeMoney(DTOUserChargeMoney chargeMoney)
        {
            var userEmail = HttpContext.User.FindFirst(ClaimTypes.Email)!.Value;

            var user = await _userService.GetUserByEmail(userEmail);

            chargeMoney.UserId = user.Id;

            await _userService.ChargeMoney(chargeMoney);

            return Ok(Constant.ChargeMoneySuccess);
        }

        [HttpGet("view-balance")]
        [Authorize(Roles = "customer")]
        public async Task<AccountBalanceVM> ViewAccountBalance()
        {
            var userEmail = HttpContext.User.FindFirst(ClaimTypes.Email)!.Value;

            var user = await _userService.GetUserByEmail(userEmail);

            return _userService.ViewAccountBalance(user.Id);
        }
    }
}

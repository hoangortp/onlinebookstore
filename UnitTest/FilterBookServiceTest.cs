﻿using Application.DTOModel.DTOBookModels;
using Application.Interfaces.IBookServices;
using Application.Services.BookServices;
using Domain.Entities;
using System.Linq.Expressions;
using Domain.Commons;

namespace UnitTest
{
    public class FilterBookServiceTest : SetupTest
    {
        private readonly IFilterBookService _filterBookService;
        public FilterBookServiceTest()
        {
            _filterBookService = new FilterBookService(_unitOfWorkMock.Object, _mapperConfig);
        }

        [Fact]
        public async Task FilterByTitleAsync_BooksExist_ReturnFilterDTOBooks()
        {
            // Arrange
            string bookTitle = "Filter";
            int pageNumber = 1;
            int pageSize = 10;

            var filteredBooks = new Pagination<Book>
            {
                Items = new List<Book>
                {
                    new Book
                    {
                        Title = "Filter 1",
                        Author = "Author",
                        Genre = "Action",
                        PublicationYear = 2000,
                        StockQuantity = 1,
                        Price = 1
                    },
                    new Book
                    {
                        Title = "Filter 2",
                        Author = "Author",
                        Genre = "Action",
                        PublicationYear = 2000,
                        StockQuantity = 1,
                        Price = 1
                    }
                },
                TotalItemCount = 2,
                PageNumber = pageNumber,
                PageSize = pageSize
            };

            var filteredDTOBooks = new Pagination<DTOBook>
            {
                Items = new List<DTOBook>
                {
                    new DTOBook
                    {
                        Title = "Filter 1",
                        Author = "Author",
                        Genre = "Action",
                        PublicationYear = 2000,
                        StockQuantity = 1,
                        Price = 1
                    },
                    new DTOBook
                    {
                        Title = "Filter 2",
                        Author = "Author",
                        Genre = "Action",
                        PublicationYear = 2000,
                        StockQuantity = 1,
                        Price = 1
                    }
                },
                TotalItemCount = 2,
                PageNumber = pageNumber,
                PageSize = pageSize
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.Filter(
            It.IsAny<Expression<Func<Book, bool>>>(), It.IsAny<int>(), It.IsAny<int>()))
            .Returns<Expression<Func<Book, bool>>, int, int>((expression, page, size) =>
                Task.FromResult((Pagination<Book>)filteredBooks));

            _mapperMock.Setup(mapper => mapper.Map<Pagination<DTOBook>>(filteredBooks)).Returns(filteredDTOBooks);

            // Act
            var result = await _filterBookService.FilterByTitleAsync(bookTitle, pageNumber, pageSize);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.Filter(
            It.IsAny<Expression<Func<Book, bool>>>(), pageNumber, pageSize), Times.Once);

            Assert.NotNull(result);
            Assert.IsType<Pagination<DTOBook>>(result);
            Assert.Equal(filteredBooks.Items.Count, result.Items.Count);
        }

        [Fact]
        public async Task FilterByAuthorAsync_BooksExist_ReturnFilterDTOBooks()
        {
            // Arrange
            string bookAuthor = "A";
            int pageNumber = 1;
            int pageSize = 10;

            var filteredBooks = new Pagination<Book>
            {
                Items = new List<Book>
                {
                    new Book
                    {
                        Title = "Test 1",
                        Author = "A",
                        Genre = "Action",
                        PublicationYear = 2000,
                        StockQuantity = 5,
                        Price = 10
                    },
                    new Book
                    {
                        Title = "Test 1",
                        Author = "A",
                        Genre = "Comedy",
                        PublicationYear = 2007,
                        StockQuantity = 10,
                        Price = 5
                    }
                },
                TotalItemCount = 2,
                PageNumber = pageNumber,
                PageSize = pageSize
            };

            var filteredDTOBooks = new Pagination<DTOBook>
            {
                Items = new List<DTOBook>
                {
                    new DTOBook
                    {
                        Title = "Test 1",
                        Author = "A",
                        Genre = "Action",
                        PublicationYear = 2000,
                        StockQuantity = 5,
                        Price = 10
                    },
                    new DTOBook
                    {
                        Title = "Test 1",
                        Author = "A",
                        Genre = "Comedy",
                        PublicationYear = 2007,
                        StockQuantity = 10,
                        Price = 5
                    }
                },
                TotalItemCount = 2,
                PageNumber = pageNumber,
                PageSize = pageSize
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.Filter(
            It.IsAny<Expression<Func<Book, bool>>>(), It.IsAny<int>(), It.IsAny<int>()))
            .Returns<Expression<Func<Book, bool>>, int, int>((expression, page, size) =>
                Task.FromResult((Pagination<Book>)filteredBooks));

            _mapperMock.Setup(mapper => mapper.Map<Pagination<DTOBook>>(filteredBooks)).Returns(filteredDTOBooks);

            // Act
            var result = await _filterBookService.FilterByAuthorAsync(bookAuthor, pageNumber, pageSize);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.Filter(
            It.IsAny<Expression<Func<Book, bool>>>(), pageNumber, pageSize), Times.Once);

            Assert.NotNull(result);
            Assert.IsType<Pagination<DTOBook>>(result);
            Assert.Equal(filteredBooks.Items.Count, result.Items.Count);
        }

        [Fact]
        public async Task FilterByYearAsync_BooksExist_ReturnFilterDTOBooks()
        {
            // Arrange
            int year = 2020;
            int pageNumber = 1;
            int pageSize = 10;

            var filteredBooks = new Pagination<Book>
            {
                Items = new List<Book>
                {
                    new Book
                    {
                        Title = "Filter 1",
                        Author = "Author",
                        Genre = "Action",
                        PublicationYear = 2020,
                        StockQuantity = 10,
                        Price = 20
                    },
                    new Book
                    {
                        Title = "Test 1",
                        Author = "Author",
                        Genre = "SciFi",
                        PublicationYear = 2020,
                        StockQuantity = 20,
                        Price = 5
                    }
                },
                TotalItemCount = 2,
                PageNumber = pageNumber,
                PageSize = pageSize
            };

            var filteredDTOBooks = new Pagination<DTOBook>
            {
                Items = new List<DTOBook>
                {
                    new DTOBook
                    {
                        Title = "Filter 1",
                        Author = "Author",
                        Genre = "Action",
                        PublicationYear = 2020,
                        StockQuantity = 10,
                        Price = 20
                    },
                    new DTOBook
                    {
                        Title = "Test 1",
                        Author = "Author",
                        Genre = "SciFi",
                        PublicationYear = 2020,
                        StockQuantity = 20,
                        Price = 5
                    }
                },
                TotalItemCount = 2,
                PageNumber = pageNumber,
                PageSize = pageSize
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.Filter(
            It.IsAny<Expression<Func<Book, bool>>>(), It.IsAny<int>(), It.IsAny<int>()))
            .Returns<Expression<Func<Book, bool>>, int, int>((expression, page, size) =>
                Task.FromResult((Pagination<Book>)filteredBooks));

            _mapperMock.Setup(mapper => mapper.Map<Pagination<DTOBook>>(filteredBooks)).Returns(filteredDTOBooks);

            // Act
            var result = await _filterBookService.FilterByYearAsync(year, pageNumber, pageSize);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.Filter(
            It.IsAny<Expression<Func<Book, bool>>>(), pageNumber, pageSize), Times.Once);

            Assert.NotNull(result);
            Assert.IsType<Pagination<DTOBook>>(result);
            Assert.Equal(filteredBooks.Items.Count, result.Items.Count);
        }

        [Fact]
        public async Task FilterByGenreAsync_BooksExist_ReturnFilterDTOBooks()
        {
            // Arrange
            string genre = "Drama";
            int pageNumber = 1;
            int pageSize = 10;

            var filteredBooks = new Pagination<Book>
            {
                Items = new List<Book>
                {
                    new Book
                    {
                        Title = "A",
                        Author = "Author 1",
                        Genre = "Drama",
                        PublicationYear = 2000,
                        StockQuantity = 1,
                        Price = 1
                    },
                    new Book
                    {
                        Title = "B",
                        Author = "Author 2",
                        Genre = "Drama",
                        PublicationYear = 2000,
                        StockQuantity = 1,
                        Price = 1
                    }
                },
                TotalItemCount = 2,
                PageNumber = pageNumber,
                PageSize = pageSize
            };

            var filteredDTOBooks = new Pagination<DTOBook>
            {
                Items = new List<DTOBook>
                {
                    new DTOBook
                    {
                        Title = "A",
                        Author = "Author 1",
                        Genre = "Drama",
                        PublicationYear = 2000,
                        StockQuantity = 1,
                        Price = 1
                    },
                    new DTOBook
                    {
                        Title = "B",
                        Author = "Author 2",
                        Genre = "Drama",
                        PublicationYear = 2000,
                        StockQuantity = 1,
                        Price = 1
                    }
                },
                TotalItemCount = 2,
                PageNumber = pageNumber,
                PageSize = pageSize
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.Filter(
            It.IsAny<Expression<Func<Book, bool>>>(), It.IsAny<int>(), It.IsAny<int>()))
            .Returns<Expression<Func<Book, bool>>, int, int>((expression, page, size) =>
                Task.FromResult((Pagination<Book>)filteredBooks));

            _mapperMock.Setup(mapper => mapper.Map<Pagination<DTOBook>>(filteredBooks)).Returns(filteredDTOBooks);

            // Act
            var result = await _filterBookService.FilterByGenreAsync(genre, pageNumber, pageSize);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.Filter(
            It.IsAny<Expression<Func<Book, bool>>>(), pageNumber, pageSize), Times.Once);

            Assert.NotNull(result);
            Assert.IsType<Pagination<DTOBook>>(result);
            Assert.Equal(filteredBooks.Items.Count, result.Items.Count);
        }
    }
}

﻿using Application;
using Application.DTOModel.DTOUser;
using Application.Interfaces.IuserServices;
using Application.Services.UserServices;
using Application.ViewModels;
using Domain.Constants;
using Domain.Entities;

namespace UnitTest
{
    public class UserServiceTest : SetupTest
    {
        private readonly IUserService _userService;

        public UserServiceTest()
        {
            _userService = new UserService(_unitOfWorkMock.Object);
        }

        [Fact]
        public async Task GetUserByEmail_UserExists_ReturnsUser()
        {
            // Arrange
            var userEmail = "user@example.com";

            var existingUser = new User
            {
                Email = userEmail,
            };

            _unitOfWorkMock.Setup(uow => uow.UserRepository.GetUserBasedOnEmail(userEmail)).ReturnsAsync(existingUser);

            // Act
            var result = await _userService.GetUserByEmail(userEmail);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.UserRepository.GetUserBasedOnEmail(userEmail), Times.Once);

            Assert.NotNull(result);
            Assert.IsType<User>(result);
            Assert.Equal(existingUser.Id, result.Id);
            Assert.Equal(existingUser.Email, result.Email);
        }

        [Fact]
        public async Task GetUserByEmail_NonUserExists_ThrowException()
        {
            // Arrange
            var userEmail = "user@example.com";

            var existingUser = new User
            {
                Email = userEmail,
            };

            _unitOfWorkMock.Setup(uow => uow.UserRepository.GetUserBasedOnEmail(userEmail)).ReturnsAsync((User)null);

            // Act
            var exception = await Assert.ThrowsAsync<Exception>(() => _userService.GetUserByEmail(userEmail));

            // Assert
            _unitOfWorkMock.Verify(uow => uow.UserRepository.GetUserBasedOnEmail(userEmail), Times.Once);

            Assert.Equal(Constant.UserNotFound, exception.Message);
        }

        [Fact]
        public async Task UpdateUser_ValidData_SuccessfullyUpdatesUserAndReturnsDTOUserUpdate()
        {
            // Arrange
            var dtoUserUpdate = new DTOUserUpdate
            {
                Id = Guid.NewGuid(),
                FullName = "UpdatedFullName",
            };

            var existingUser = new User
            {
                FullName = "OriginalFullName",
            };

            _unitOfWorkMock.Setup(uow => uow.UserRepository.GetById(dtoUserUpdate.Id)).Returns(existingUser);
            _unitOfWorkMock.Setup(uow => uow.UserRepository.Update(existingUser));
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(1);

            // Act
            var result = await _userService.UpdateUser(dtoUserUpdate);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.UserRepository.GetById(dtoUserUpdate.Id), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.UserRepository.Update(existingUser), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);

            Assert.NotNull(result);
            Assert.IsType<DTOUserUpdate>(result);
            Assert.Equal(dtoUserUpdate.FullName, result.FullName);
        }

        [Fact]
        public async Task UpdateUser_SaveChangeFails_ReturnsNull()
        {
            // Arrange
            var dtoUserUpdate = new DTOUserUpdate
            {
                Id = Guid.NewGuid(),
                FullName = "UpdatedFullName",
            };

            var existingUser = new User
            {
                FullName = "OriginalFullName",
            };

            _unitOfWorkMock.Setup(uow => uow.UserRepository.GetById(dtoUserUpdate.Id)).Returns(existingUser);
            _unitOfWorkMock.Setup(uow => uow.UserRepository.Update(existingUser));
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(0);

            // Act
            var result = await _userService.UpdateUser(dtoUserUpdate);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.UserRepository.GetById(dtoUserUpdate.Id), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.UserRepository.Update(existingUser), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);

            Assert.Null(result);
        }

        [Fact]
        public async Task ChargeMoney_ValidData_SuccessfullyUpdatesUserAccountBalance()
        {
            // Arrange
            var chargeMoney = new DTOUserChargeMoney
            {
                UserId = Guid.NewGuid(),
                Money = 100.0m,
            };

            var existingUser = new User
            {
                AccountBalance = 50.0m,
            };

            _unitOfWorkMock.Setup(uow => uow.UserRepository.GetById(chargeMoney.UserId)).Returns(existingUser);
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(1);

            // Act
            await _userService.ChargeMoney(chargeMoney);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.UserRepository.GetById(chargeMoney.UserId), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);

            Assert.Equal(150.0m, existingUser.AccountBalance);
        }

        [Fact]
        public void ViewAccountBalance_UserExists_ReturnsAccountBalanceVM()
        {
            // Arrange
            var userId = Guid.NewGuid();

            var existingUser = new User
            {
                AccountBalance = 100.0m,
            };

            _unitOfWorkMock.Setup(uow => uow.UserRepository.GetById(userId)).Returns(existingUser);

            // Act
            var result = _userService.ViewAccountBalance(userId);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.UserRepository.GetById(userId), Times.Once);

            Assert.NotNull(result);
            Assert.IsType<AccountBalanceVM>(result);
            Assert.Equal(existingUser.AccountBalance, result.AccountBalance);
        }
    }
}

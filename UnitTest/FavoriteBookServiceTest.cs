﻿using Application;
using Application.DTOModel;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels;
using AutoMapper;
using Domain.Commons;
using Domain.Constants;
using Domain.Entities;

namespace UnitTest
{
    public class FavoriteBookServiceTest : SetupTest
    {
        private readonly IFavoriteBookService _favoriteBookService;
        public FavoriteBookServiceTest()
        {
            _favoriteBookService = new FavoriteBookService(_unitOfWorkMock.Object);
        }

        [Fact]
        public async Task AddFavoriteBook_BookAndUserExist_ReturnsDTOFavoriteBook()
        {
            // Arrange
            var dtoFavoriteBook = new DTOFavoriteBook
            {
                BookId = Guid.NewGuid(),
                UserId = Guid.NewGuid(),
            };

            var existingBook = new Book
            {
                Title = "Existing Book",
            };

            var existingUser = new User
            {
                Email = "Existing Email",
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetById(dtoFavoriteBook.BookId)).Returns(existingBook);
            _unitOfWorkMock.Setup(uow => uow.UserRepository.GetById(dtoFavoriteBook.UserId)).Returns(existingUser);
            _unitOfWorkMock.Setup(uow => uow.FavoriteBookRepository
                                            .GetFavoriteBookByUserIdAndBookId(dtoFavoriteBook.UserId, dtoFavoriteBook.BookId))
                                            .ReturnsAsync((FavoriteBook)null);
            _unitOfWorkMock.Setup(uow => uow.FavoriteBookRepository.AddAsync(It.IsAny<FavoriteBook>()));
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(1);

            //Act
            var result = await _favoriteBookService.AddFavoriteBook(dtoFavoriteBook);

            //Assert
            _unitOfWorkMock.Verify(uow => uow.FavoriteBookRepository.AddAsync(It.IsAny<FavoriteBook>()), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);   

            Assert.NotNull(result);
            Assert.IsType<DTOFavoriteBook>(result);
        }

        [Fact]
        public async Task AddFavoriteBook_BookNotFound_ThrowsException()
        {
            //Arrange
            var dtoFavoriteBook = new DTOFavoriteBook
            {
                BookId = Guid.NewGuid(),
                UserId = Guid.NewGuid(),
                // Set other properties as needed
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetById(dtoFavoriteBook.BookId)).Returns((Book)null);

            //Act
            var exception = await Assert.ThrowsAsync<Exception>(() => _favoriteBookService.AddFavoriteBook(dtoFavoriteBook));

            //Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetById(dtoFavoriteBook.BookId), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.UserRepository.GetById(It.IsAny<Guid>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.FavoriteBookRepository.GetFavoriteBookByUserIdAndBookId(It.IsAny<Guid>(), It.IsAny<Guid>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.FavoriteBookRepository.AddAsync(It.IsAny<FavoriteBook>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Never);

            Assert.Equal(Constant.BookNotFound, exception.Message);
        }

        [Fact]
        public async Task AddFavoriteBook_UserNotFound_ThrowsException()
        {
            //Arrange
            var dtoFavoriteBook = new DTOFavoriteBook
            {
                BookId = Guid.NewGuid(),
                UserId = Guid.NewGuid(),
            };

            var existingBook = new Book
            {
                Title = "Existing Book",
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetById(dtoFavoriteBook.BookId)).Returns(existingBook);
            _unitOfWorkMock.Setup(uow => uow.UserRepository.GetById(dtoFavoriteBook.UserId)).Returns((User)null);

            //Act
            var exception = await Assert.ThrowsAsync<Exception>(() => _favoriteBookService.AddFavoriteBook(dtoFavoriteBook));

            //Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetById(dtoFavoriteBook.BookId), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.UserRepository.GetById(It.IsAny<Guid>()), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.FavoriteBookRepository.GetFavoriteBookByUserIdAndBookId(It.IsAny<Guid>(), It.IsAny<Guid>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.FavoriteBookRepository.AddAsync(It.IsAny<FavoriteBook>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Never);

            Assert.Equal(Constant.UserNotFound, exception.Message);
        }

        [Fact]
        public async Task ViewFavoriteBooks_UserAndFavoriteBooksExist_ReturnsPaginationOfFavoriteBookVM()
        {
            // Arrange
            var userId = Guid.NewGuid();
            var bookTitle = "Favorite Book Title";
            var pageNumber = 1;
            var pageSize = 10;

            var existingUser = new User
            {
                Email = "Existing Email",
            };

            var favoriteBooks = new Pagination<FavoriteBook>
            {
                Items = new List<FavoriteBook>
            {
                new FavoriteBook
                {
                    UserId = userId,
                    BookId = Guid.NewGuid(),
                },
                new FavoriteBook
                {
                    UserId = userId,
                    BookId = Guid.NewGuid(),
                },
            },
                TotalItemCount = 2,
                PageNumber = pageNumber,
                PageSize = pageSize,
            };


            _unitOfWorkMock.Setup(uow => uow.UserRepository.GetById(userId)).Returns(existingUser);
            _unitOfWorkMock.Setup(uow => uow.FavoriteBookRepository.Paging(pageNumber, pageSize)).ReturnsAsync(favoriteBooks);
            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetBookTitleById(It.IsAny<Guid>())).Returns(bookTitle);

            // Act
            var result = await _favoriteBookService.ViewFavoriteBooks(userId, pageNumber, pageSize);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.UserRepository.GetById(userId), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.FavoriteBookRepository.Paging(pageNumber, pageSize), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetBookTitleById(It.IsAny<Guid>()), Times.Exactly(favoriteBooks.Items.Count));

            Assert.NotNull(result);
            Assert.IsType<Pagination<FavoriteBookVM>>(result);
            Assert.Equal(favoriteBooks.Items.Count, result.Items.Count);
        }

        [Fact]
        public async Task ViewFavoriteBooks_UserNotFound_ThrowsException()
        {
            // Arrange
            var userId = Guid.NewGuid();
            var pageNumber = 1;
            var pageSize = 10;

            _unitOfWorkMock.Setup(uow => uow.UserRepository.GetById(userId)).Returns((User)null);

            // Act and Assert
            var exception = await Assert.ThrowsAsync<Exception>(() => _favoriteBookService.ViewFavoriteBooks(userId, pageNumber, pageSize));

            // Verify
            _unitOfWorkMock.Verify(uow => uow.UserRepository.GetById(userId), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.FavoriteBookRepository.Paging(It.IsAny<int>(), It.IsAny<int>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetBookTitleById(It.IsAny<Guid>()), Times.Never);

            Assert.Equal(Constant.UserNotFound, exception.Message);
        }

        [Fact]
        public async Task RemoveFavoriteBook_ExistingFavoriteBook_SoftDeletesAndReturnsDTOFavoriteBook()
        {
            // Arrange
            var dtoFavoriteBook = new DTOFavoriteBook
            {
                UserId = Guid.NewGuid(),
                BookId = Guid.NewGuid(),
            };

            var existingFavoriteBook = new FavoriteBook
            {
                UserId = dtoFavoriteBook.UserId,
                BookId = dtoFavoriteBook.BookId,
                IsDeleted = false,
            };

            _unitOfWorkMock.Setup(uow => uow.FavoriteBookRepository
                .GetFavoriteBookByUserIdAndBookId(dtoFavoriteBook.UserId, dtoFavoriteBook.BookId))
                .ReturnsAsync(existingFavoriteBook);
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(1);

            // Act
            var result = await _favoriteBookService.RemoveFavoriteBook(dtoFavoriteBook);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.FavoriteBookRepository
                .GetFavoriteBookByUserIdAndBookId(dtoFavoriteBook.UserId, dtoFavoriteBook.BookId), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.FavoriteBookRepository.SoftDelete(existingFavoriteBook), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);

            Assert.NotNull(result);
            Assert.IsType<DTOFavoriteBook>(result);
        }
    }
}

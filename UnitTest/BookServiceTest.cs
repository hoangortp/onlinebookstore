﻿using Application.DTOModel.DTOBookModels;
using Application.Interfaces.IBookServices;
using Application.Services.BookServices;
using Domain.Commons;
using Domain.Constants;
using Domain.Entities;

namespace UnitTest
{
    public class BookServiceTest : SetupTest
    {
        private readonly IBookService _bookService;

        public BookServiceTest()
        {
            _bookService = new BookService(_unitOfWorkMock.Object, _mapperConfig);
        }

        [Fact]
        public void GetBookById_WithExistingBook_ReturnsDTOBook()
        {
            // Arrange
            var bookId = Guid.NewGuid();
            var expectedBook = new Book
            {
                Title = "Test Title",
                Author = "Test Author",
                Genre = "Fiction",
                PublicationYear = 2022,
                StockQuantity = 10,
                Price = 1,
                CreatedAt = DateTime.Now,
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetById(It.IsAny<Guid>())).Returns(expectedBook);

            // Act
            var result = _bookService.GetBookById(bookId);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetById(It.IsAny<Guid>()), Times.Once);
            Assert.NotNull(result);
            Assert.IsType<DTOBook>(result);
        }

        [Fact]
        public void GetBookById_WithNonExistingBook_ThrowException()
        {
            // Arrange
            var bookId = Guid.NewGuid();

            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetById(It.IsAny<Guid>())).Returns((Book)null);

            // Act
            var exception = Assert.Throws<Exception>(() => _bookService.GetBookById(bookId));

            // Assert
            Assert.Equal(Constant.BookNotFound, exception.Message);
        }

        [Fact]
        public async Task GetBooks_ReturnPaginationDTOBooks()
        {
            // Arrange
            var pageNumber = 1;
            var pageSize = 3;

            var paginationBooks = new Pagination<Book>
            {
                Items = new List<Book>
                {
                    new Book
                    {
                        Title = "Harry Potter and the Philosopher's Stone",
                        Author = "J. K. Rowling",
                        Genre = "Fantasy",
                        PublicationYear = 1997,
                        StockQuantity = 20,
                        Price = 5,
                        IsDeleted = false,
                    },
                    new Book
                    {
                        Title = "Harry Potter and the Chamber of Secret",
                        Author = "J. K. Rowling",
                        Genre = "Fantasy",
                        PublicationYear = 1998,
                        StockQuantity = 20,
                        Price = 5,
                        IsDeleted = false,
                    },
                    new Book
                    {
                        Title = "Harry Potter and the Prisoner of Azkaban",
                        Author = "J. K. Rowling",
                        Genre = "Fantasy",
                        PublicationYear = 1999,
                        StockQuantity = 20,
                        Price = 5,
                        IsDeleted = false,
                    },
                },
                TotalItemCount = 15,
                PageNumber = pageNumber,
                PageSize = pageSize,
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.Paging(pageNumber, pageSize)).ReturnsAsync(paginationBooks);

            var paginationDTOBooks = new Pagination<DTOBook>
            {
                Items = new List<DTOBook>
                {
                    new DTOBook
                    {
                        Title = "Harry Potter and the Philosopher's Stone",
                        Author = "J. K. Rowling",
                        Genre = "Fantasy",
                        PublicationYear = 1997,
                        StockQuantity = 20,
                        Price = 5,
                    },
                    new DTOBook
                    {
                        Title = "Harry Potter and the Chamber of Secret",
                        Author = "J. K. Rowling",
                        Genre = "Fantasy",
                        PublicationYear = 1998,
                        StockQuantity = 20,
                        Price = 5,
                    },
                    new DTOBook
                    {
                        Title = "Harry Potter and the Prisoner of Azkaban",
                        Author = "J. K. Rowling",
                        Genre = "Fantasy",
                        PublicationYear = 1999,
                        StockQuantity = 20,
                        Price = 5,
                    },
                },
                TotalItemCount = 15,
                PageNumber = pageNumber,
                PageSize = pageSize,
            };

            _mapperMock.Setup(mapper => mapper.Map<Pagination<DTOBook>>(paginationBooks)).Returns(paginationDTOBooks);

            // Act
            var result = await _bookService.GetBooks(pageNumber, pageSize);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.Paging(pageNumber, pageSize), Times.Once);
            Assert.NotNull(result);
            Assert.IsType<Pagination<DTOBook>>(result);
            Assert.Equal(paginationDTOBooks.Items.Count, result.Items.Count);
            Assert.Equal(paginationDTOBooks.TotalItemCount, result.TotalItemCount);
        }

        [Fact]
        public async Task AddBook_ValidInput_ReturnDTOBook()
        {
            // Arrange
            string title = "Test Title";
            string author = "Test Author";
            string genre = "Comedy";
            int year = 2000;
            int stockQuantity = 1;
            decimal price = 5;


            DTOCreatedBook createdBook = new DTOCreatedBook
            {
                Title = title,
                Author = author,
                Genre = genre,
                PublicationYear = year,
                StockQuantity = stockQuantity,
                Price = price,
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.AddAsync(It.IsAny<Book>()));
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(1);

            // Act
            var result = await _bookService.AddBook(createdBook);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.AddAsync(It.IsAny<Book>()), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);
            Assert.NotNull(result);
            Assert.IsType<DTOBook>(result);
        }

        [Fact]
        public async Task AddBook_InvalidGenre_ThrowException()
        {
            // Arrange
            string title = "Test Title";
            string author = "Test Author";
            string genre = "A";
            int year = 2000;
            int stockQuantity = 1;
            decimal price = 5;


            DTOCreatedBook createdBook = new DTOCreatedBook
            {
                Title = title,
                Author = author,
                Genre = genre,
                PublicationYear = year,
                StockQuantity = stockQuantity,
                Price = price,
            };

            // Act
            var exception = await Assert.ThrowsAsync<Exception>(() => _bookService.AddBook(createdBook));

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.AddAsync(It.IsAny<Book>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Never);
            Assert.Equal(Constant.GenreInvalid, exception.Message);
        }

        [Fact]
        public async Task AddBook_SaveChangeFails_ReturnNull()
        {
            // Arrange
            string title = "Test Title";
            string author = "Test Author";
            string genre = "Comedy";
            int year = 2000;
            int stockQuantity = 1;
            decimal price = 5;


            DTOCreatedBook createdBook = new DTOCreatedBook
            {
                Title = title,
                Author = author,
                Genre = genre,
                PublicationYear = year,
                StockQuantity = stockQuantity,
                Price = price,
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.AddAsync(It.IsAny<Book>()));
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(0);

            // Act
            var result = await _bookService.AddBook(createdBook);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.AddAsync(It.IsAny<Book>()), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);
            Assert.Null(result);
        }

        [Fact]
        public async Task AddRangeBook_ValidInput_ReturnListDTOBook()
        {
            // Arrange
            string title = "Test Title";
            string author = "Test Author";
            string genre = "Comedy";
            int year = 2000;
            int stockQuantity = 1;
            decimal price = 5;

            var dtoCreatedBooks = new List<DTOCreatedBook>
            {
                new DTOCreatedBook
                {
                    Title = title,
                    Author = author,
                    Genre = genre,
                    PublicationYear = year,
                    StockQuantity = stockQuantity,
                    Price = price
                },
                new DTOCreatedBook
                {
                    Title = title,
                    Author = author,
                    Genre = genre,
                    PublicationYear = year,
                    StockQuantity = stockQuantity,
                    Price = price
                },
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.AddRangeAsync(It.IsAny<ICollection<Book>>()));
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(1);

            // Act
            var result = await _bookService.AddRangeBook(dtoCreatedBooks);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.AddRangeAsync(It.IsAny<ICollection<Book>>()), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);
            Assert.NotNull(result);
            Assert.IsType<List<DTOBook>>(result);
        }

        [Fact]
        public async Task AddRangeBook_InvalidGenre_ThrowException()
        {
            // Arrange
            string title = "Test Title";
            string author = "Test Author";
            string genre = "B";
            int year = 2000;
            int stockQuantity = 1;
            decimal price = 5;

            var dtoCreatedBooks = new List<DTOCreatedBook>
            {
                new DTOCreatedBook
                {
                    Title = title,
                    Author = author,
                    Genre = genre,
                    PublicationYear = year,
                    StockQuantity = stockQuantity,
                    Price = price
                },
                new DTOCreatedBook
                {
                    Title = title,
                    Author = author,
                    Genre = genre,
                    PublicationYear = year,
                    StockQuantity = stockQuantity,
                    Price = price
                },
            };

            // Act
            var exception = await Assert.ThrowsAsync<Exception>(() => _bookService.AddRangeBook(dtoCreatedBooks));

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.AddRangeAsync(It.IsAny<ICollection<Book>>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Never);
            Assert.Equal(Constant.GenreInvalid, exception.Message);
        }

        [Fact]
        public async Task AddRangeBook_SaveChangeFails_ReturnNull()
        {
            // Arrange
            string title = "Test Title";
            string author = "Test Author";
            string genre = "Comedy";
            int year = 2000;
            int stockQuantity = 1;
            decimal price = 5;

            var dtoCreatedBooks = new List<DTOCreatedBook>
            {
                new DTOCreatedBook
                {
                    Title = title,
                    Author = author,
                    Genre = genre,
                    PublicationYear = year,
                    StockQuantity = stockQuantity,
                    Price = price
                },
                new DTOCreatedBook
                {
                    Title = title,
                    Author = author,
                    Genre = genre,
                    PublicationYear = year,
                    StockQuantity = stockQuantity,
                    Price = price
                },
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.AddRangeAsync(It.IsAny<ICollection<Book>>()));
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(0);

            // Act
            var result = await _bookService.AddRangeBook(dtoCreatedBooks);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.AddRangeAsync(It.IsAny<ICollection<Book>>()), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);
            Assert.Null(result);
        }

        [Fact]
        public async Task SoftDeleteBook_ExistingBook_DeleteAndReturnsDTOBook()
        {
            // Arrange
            var bookId = Guid.NewGuid();
            string title = "Test Title";
            string author = "Test Author";
            string genre = "Action";
            int year = 2000;
            int stockQuantity = 1;
            decimal price = 5;

            var existingBook = new Book
            {
                Title = title,
                Author = author,
                Genre = genre,
                PublicationYear = year,
                StockQuantity = stockQuantity,
                Price = price
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetById(bookId)).Returns(existingBook);
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(1);

            // Act
            var result = await _bookService.SoftDeleteBook(bookId);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetById(bookId), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.BookRepository.SoftDelete(existingBook), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);
            Assert.NotNull(result);
            Assert.IsType<DTOBook>(result);
        }

        [Fact]
        public async Task SoftDeleteBook_NonExistingBook_ThrowException()
        {
            // Arrange
            var bookId = Guid.NewGuid();

            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetById(bookId)).Returns((Book)null);

            // Act
            var exception = await Assert.ThrowsAsync<Exception>(() => _bookService.SoftDeleteBook(bookId));

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetById(bookId), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.BookRepository.SoftDelete(It.IsAny<Book>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Never);
            Assert.Equal(Constant.BookNotFound, exception.Message);
        }

        [Fact]
        public async Task SoftDeleteRangeBook_ExistingBooks_DeletesAndReturnsDTOBooks()
        {
            // Arrange
            var idsToDelete = new List<Guid> { Guid.NewGuid(), Guid.NewGuid() };

            var books = new List<Book>
            {
                new Book(),
                new Book()
            };

            var deletedBooks = books.Where(x => idsToDelete.Contains(x.Id)).ToList();

            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetAllAsync()).ReturnsAsync(books);
            _unitOfWorkMock.Setup(uow => uow.BookRepository.SoftDeleteRange(It.IsAny<ICollection<Book>>()));
            _unitOfWorkMock.Setup(u => u.SaveChangeAsync()).ReturnsAsync(1);


            // Act
            var result = await _bookService.SoftDeleteRangeBook(idsToDelete);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetAllAsync(), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);
            Assert.NotNull(result);
            Assert.IsType<List<DTOBook>>(result);
            Assert.Equal(deletedBooks.Count, result.Count);
        }

        [Fact]
        public async Task SoftDeleteRangeBook_SoftDeleteFails_ReturnsNull()
        {
            // Arrange
            var idsToDelete = new List<Guid> { Guid.NewGuid(), Guid.NewGuid() };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetAllAsync()).ReturnsAsync(new List<Book>());
            _unitOfWorkMock.Setup(uow => uow.BookRepository.SoftDeleteRange(It.IsAny<ICollection<Book>>()));
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(0);

            // Act
            var result = await _bookService.SoftDeleteRangeBook(idsToDelete);

            // Assert
            Assert.Null(result);
            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetAllAsync(), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.BookRepository.SoftDeleteRange(It.IsAny<ICollection<Book>>()), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);
        }

        [Fact]
        public async Task UpdateBook_BookExists_UpdatesAndReturnsDTOBook()
        {
            // Arrange
            var bookId = Guid.NewGuid();

            var dtoBook = new DTOBook
            {
                Id = bookId,
                Title = "Updated Title",
                Author = "Updated Author",
                Genre = "Updated Genre",
                PublicationYear = 2022,
                StockQuantity = 50,
                Price = 15
            };

            var existingBook = new Book
            {
                Title = "Original Title",
                Author = "Original Author",
                Genre = "Original Genre",
                PublicationYear = 2021,
                StockQuantity = 30,
                Price = 10,
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetById(dtoBook.Id)).Returns(existingBook);
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(1);


            // Act
            var result = await _bookService.UpdateBook(dtoBook);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetById(dtoBook.Id), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.BookRepository.Update(existingBook), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);

            Assert.NotNull(result);
            Assert.IsType<DTOBook>(result);
            Assert.Equal(dtoBook.Title, result.Title);
            Assert.Equal(dtoBook.Author, result.Author);
            Assert.Equal(dtoBook.Genre, result.Genre);
            Assert.Equal(dtoBook.PublicationYear, result.PublicationYear);
            Assert.Equal(dtoBook.StockQuantity, result.StockQuantity);
            Assert.Equal(dtoBook.Price, result.Price);
        }

        [Fact]
        public async Task UpdateBook_NonExistingBook_ThrowException()
        {
            // Arrange
            var bookId = Guid.NewGuid();

            var dtoBook = new DTOBook
            {
                Id = bookId,
                Title = "Updated Title",
                Author = "Updated Author",
                Genre = "Updated Genre",
                PublicationYear = 2022,
                StockQuantity = 50,
                Price = 15
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetById(dtoBook.Id)).Returns((Book)null);

            // Act
            var exception = await Assert.ThrowsAsync<Exception>(() => _bookService.UpdateBook(dtoBook));

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetById(It.IsAny<Guid>()), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.BookRepository.Update(It.IsAny<Book>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Never);
            Assert.Equal(Constant.BookNotFound, exception.Message);

        }

        [Fact]
        public async Task UpdateBook_SaveChangeFails_ReturnsNull()
        {
            // Arrange
            var bookId = Guid.NewGuid();

            var dtoBook = new DTOBook
            {
                Id = bookId,
                Title = "Updated Title",
                Author = "Updated Author",
                Genre = "Updated Genre",
                PublicationYear = 2022,
                StockQuantity = 50,
                Price = 15
            };

            var existingBook = new Book
            {
                Title = "Original Title",
                Author = "Original Author",
                Genre = "Original Genre",
                PublicationYear = 2021,
                StockQuantity = 30,
                Price = 10,
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetById(dtoBook.Id)).Returns(existingBook);
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(0);


            // Act
            var result = await _bookService.UpdateBook(dtoBook);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetById(It.IsAny<Guid>()), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);
            Assert.Null(result);
        }
    }
}

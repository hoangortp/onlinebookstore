﻿using Application.DTOModel.DTOUserModels;
using Application.Interfaces.IuserServices;
using Application.Services.UserServices;
using Domain.Entities;
using Domain.Constants;
using Application.Utils.AuthUtils;

namespace UnitTest
{
    public class AuthServiceTest : SetupTest
    {
        private readonly IAuthService _authService;

        public AuthServiceTest()
        {
            _authService = new AuthService(_unitOfWorkMock.Object,
                                           _mapperConfig,
                                           _currentTimeServiceMock.Object,
                                           _appConfiguration,
                                           _otpServiceMock.Object
                                            );
        }

        [Fact]
        public async Task RegisterAsync_NewUser_SuccessfullyRegistersAndReturnsDTOUserRegister()
        {
            // Arrange
            var dtoUserRegister = new DTOUserRegister
            {
                FullName = "John Doe",
                Email = "john.doe@example.com",
                Role = "customer",
                Password = "Password123",
            };

            _unitOfWorkMock.Setup(uow => uow.UserRepository.CheckEmailExisted(dtoUserRegister.Email)).ReturnsAsync(false);

            var salt = "GeneratedSalt";
            var hashedPassword = "GeneratedHashedPassword";

            var newUser = new User
            {
                FullName = dtoUserRegister.FullName,
                Email = dtoUserRegister.Email,
                Role = dtoUserRegister.Role,
                Salt = salt,
                HashedPassword = hashedPassword
            };

            _unitOfWorkMock.Setup(uow => uow.UserRepository.AddAsync(It.IsAny<User>()));
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(1);

            // Act
            var result = await _authService.RegisterAsync(dtoUserRegister);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.UserRepository.CheckEmailExisted(dtoUserRegister.Email), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.UserRepository.AddAsync(It.IsAny<User>()), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);

            Assert.NotNull(result);
            Assert.IsType<DTOUserRegister>(result);
        }

        [Fact]
        public async Task RegisterAsync_EmailExists_ThrowsException()
        {
            // Arrange
            var dtoUserRegister = new DTOUserRegister
            {
                FullName = "John Doe",
                Email = "john.doe@example.com",
                Role = "User",
                Password = "Password123",
            };

            _unitOfWorkMock.Setup(uow => uow.UserRepository.CheckEmailExisted(dtoUserRegister.Email)).ReturnsAsync(true);

            // Act and Assert
            var exception = await Assert.ThrowsAsync<Exception>(() => _authService.RegisterAsync(dtoUserRegister));

            // Verify
            _unitOfWorkMock.Verify(uow => uow.UserRepository.CheckEmailExisted(dtoUserRegister.Email), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.UserRepository.AddAsync(It.IsAny<User>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Never);

            Assert.Equal(Constant.EmailExists, exception.Message);
        }

        [Fact]
        public async Task RegisterAsync_SaveChangeAsyncFails_ReturnsNull()
        {
            // Arrange
            var dtoUserRegister = new DTOUserRegister
            {
                FullName = "John Doe",
                Email = "john.doe@example.com",
                Role = "User",
                Password = "Password123",
            };

            _unitOfWorkMock.Setup(uow => uow.UserRepository.CheckEmailExisted(dtoUserRegister.Email)).ReturnsAsync(false);

            var newUser = new User
            {
                FullName = dtoUserRegister.FullName,
                Email = dtoUserRegister.Email,
                Role = dtoUserRegister.Role,
                Salt = "GeneratedSalt",
                HashedPassword = "GeneratedHashedPassword"
            };

            _unitOfWorkMock.Setup(uow => uow.UserRepository.AddAsync(It.IsAny<User>()));
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(0);

            // Act
            var result = await _authService.RegisterAsync(dtoUserRegister);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.UserRepository.CheckEmailExisted(dtoUserRegister.Email), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.UserRepository.AddAsync(It.IsAny<User>()), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);

            Assert.Null(result);
        }

        [Fact]
        public async Task ResetPassword_ValidData_SuccessfullyResetsPasswordAndReturnsDTOUserResetPassword()
        {
            // Arrange
            var dtoUserResetPassword = new DTOUserResetPassword
            {
                Email = "user@example.com",
                NewPassword = "NewPassword123",
                OTP = 123456,
            };

            var user = new User
            {
                Email = dtoUserResetPassword.Email,
                Salt = "GeneratedSalt",
                HashedPassword = PasswordUtils.HashWithSalt("OldPassword123", "GeneratedSalt"),
            };

            _unitOfWorkMock.Setup(uow => uow.UserRepository.GetUserBasedOnEmail(dtoUserResetPassword.Email)).ReturnsAsync(user);
            _otpServiceMock.Setup(otp => otp.VerifyOTP(dtoUserResetPassword.Email, dtoUserResetPassword.OTP, _currentTimeServiceMock.Object)).ReturnsAsync(true);
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(1);

            // Act
            var result = await _authService.ResetPassword(dtoUserResetPassword);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.UserRepository.GetUserBasedOnEmail(dtoUserResetPassword.Email), Times.Once);
            _otpServiceMock.Verify(otp => otp.VerifyOTP(dtoUserResetPassword.Email, dtoUserResetPassword.OTP, _currentTimeServiceMock.Object), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.UserRepository.Update(user), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);

            Assert.NotNull(result);
            Assert.IsType<DTOUserResetPassword>(result);
            Assert.Equal(dtoUserResetPassword.Email, result.Email);
            Assert.Equal(dtoUserResetPassword.OTP, result.OTP);
        }

        [Fact]
        public async Task ResetPassword_SaveChangeFails_ReturnNull()
        {
            // Arrange
            var dtoUserResetPassword = new DTOUserResetPassword
            {
                Email = "user@example.com",
                NewPassword = "NewPassword123",
                OTP = 123456,
            };

            var user = new User
            {
                Email = dtoUserResetPassword.Email,
                Salt = "GeneratedSalt",
                HashedPassword = PasswordUtils.HashWithSalt("OldPassword123", "GeneratedSalt"),
            };

            _unitOfWorkMock.Setup(uow => uow.UserRepository.GetUserBasedOnEmail(dtoUserResetPassword.Email)).ReturnsAsync(user);
            _otpServiceMock.Setup(otp => otp.VerifyOTP(dtoUserResetPassword.Email, dtoUserResetPassword.OTP, _currentTimeServiceMock.Object)).ReturnsAsync(true);
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(0);

            // Act
            var result = await _authService.ResetPassword(dtoUserResetPassword);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.UserRepository.GetUserBasedOnEmail(dtoUserResetPassword.Email), Times.Once);
            _otpServiceMock.Verify(otp => otp.VerifyOTP(dtoUserResetPassword.Email, dtoUserResetPassword.OTP, _currentTimeServiceMock.Object), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.UserRepository.Update(user), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);

            Assert.Null(result);
        }
    }
}

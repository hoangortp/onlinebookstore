﻿using Application;
using Application.DTOModel;
using Application.Interfaces;
using Application.Services.CartItemServices;
using Application.ViewModels;
using Domain.Commons;
using Domain.Constants;
using Domain.Entities;
using Moq;

namespace UnitTest
{
    public class CartItemServiceTest : SetupTest
    {
        private readonly ICartItemService _cartItemService;

        public CartItemServiceTest()
        {
            _cartItemService = new CartItemService(_unitOfWorkMock.Object, _mapperConfig);
        }

        [Fact]
        public async Task AddToCart_NewCartItem_SuccessfullyAddsToCart()
        {
            // Arrange
            var dtoCartItem = new DTOCartItem
            {
                BookId = Guid.NewGuid(),
                UserId = Guid.NewGuid(),
                Quantity = 2,
            };

            var existingBook = new Book
            {
                StockQuantity = 5,
                Price = 20.0m,
            };

            var existingUser = new User
            {
                FullName = "Test User",
                Email = "abc@gmail.com"
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetById(dtoCartItem.BookId)).Returns(existingBook);
            _unitOfWorkMock.Setup(uow => uow.UserRepository.GetById(dtoCartItem.UserId)).Returns(existingUser);
            _unitOfWorkMock.Setup(uow => uow.CartItemRepository.GetCartItemByUserIdAndBookId(dtoCartItem.UserId, dtoCartItem.BookId)).ReturnsAsync((CartItem)null);
            _unitOfWorkMock.Setup(uow => uow.CartItemRepository.AddAsync(It.IsAny<CartItem>()));
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(1);

            // Act
            var result = await _cartItemService.AddToCart(dtoCartItem);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetById(dtoCartItem.BookId), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.UserRepository.GetById(dtoCartItem.UserId), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.CartItemRepository.AddAsync(It.IsAny<CartItem>()), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);

            Assert.NotNull(result);
            Assert.IsType<DTOCartItem>(result);
            Assert.Equal(dtoCartItem.BookId, result.BookId);
            Assert.Equal(dtoCartItem.UserId, result.UserId);
            Assert.Equal(dtoCartItem.Quantity, result.Quantity);
        }

        [Fact]
        public async Task AddToCart_ExistingCartItem_SuccessfullyUpdatesCartItem()
        {
            // Arrange
            var dtoCartItem = new DTOCartItem
            {
                BookId = Guid.NewGuid(),
                UserId = Guid.NewGuid(),
                Quantity = 2,
            };

            var existingBook = new Book
            {
                StockQuantity = 5,
                Price = 20.0m,
            };

            var existingUser = new User
            {
                FullName = "Test User",
                Email = "abc@gmail.com"
            };

            var existingCartItem = new CartItem
            {
                BookId = dtoCartItem.BookId,
                UserId = dtoCartItem.UserId,
                Quantity = 1,
                SubtotalPrice = 20.0m,
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetById(dtoCartItem.BookId)).Returns(existingBook);
            _unitOfWorkMock.Setup(uow => uow.UserRepository.GetById(dtoCartItem.UserId)).Returns(existingUser);
            _unitOfWorkMock.Setup(uow => uow.CartItemRepository.GetCartItemByUserIdAndBookId(dtoCartItem.UserId, dtoCartItem.BookId)).ReturnsAsync(existingCartItem);
            _unitOfWorkMock.Setup(uow => uow.CartItemRepository.Update(It.IsAny<CartItem>()));
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(1);

            // Act
            var result = await _cartItemService.AddToCart(dtoCartItem);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetById(dtoCartItem.BookId), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.UserRepository.GetById(dtoCartItem.UserId), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);

            Assert.NotNull(result);
            Assert.IsType<DTOCartItem>(result);
            Assert.Equal(dtoCartItem.BookId, result.BookId);
            Assert.Equal(dtoCartItem.UserId, result.UserId);
            Assert.Equal(existingCartItem.Quantity + dtoCartItem.Quantity, result.Quantity);
        }

        [Fact]
        public async Task AddToCart_InsufficientStock_ThrowsException()
        {
            // Arrange
            var dtoCartItem = new DTOCartItem
            {
                BookId = Guid.NewGuid(),
                UserId = Guid.NewGuid(),
                Quantity = 5,
            };

            var existingBook = new Book
            {
                StockQuantity = 2,
                Price = 20.0m,
            };

            var existingUser = new User
            {
                FullName = "Test User",
                Email = "abc@gmail.com"
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetById(dtoCartItem.BookId)).Returns(existingBook);
            _unitOfWorkMock.Setup(uow => uow.UserRepository.GetById(dtoCartItem.UserId)).Returns(existingUser);


            // Act
            var exception = await Assert.ThrowsAsync<Exception>(() => _cartItemService.AddToCart(dtoCartItem));

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetById(dtoCartItem.BookId), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.UserRepository.GetById(dtoCartItem.UserId), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.CartItemRepository.GetCartItemByUserIdAndBookId(dtoCartItem.UserId, dtoCartItem.BookId), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.CartItemRepository.AddAsync(It.IsAny<CartItem>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Never);

            Assert.Equal(Constant.CannotHigherThanStock, exception.Message);
        }

        [Fact]
        public async Task AddToCart_SaveChangeFails_ReturnsNull()
        {
            // Arrange
            var dtoCartItem = new DTOCartItem
            {
                BookId = Guid.NewGuid(),
                UserId = Guid.NewGuid(),
                Quantity = 2,
            };

            var existingBook = new Book
            {
                StockQuantity = 5,
                Price = 20.0m,
            };

            var existingUser = new User
            {
                FullName = "Test User",
                Email = "abc@gmail.com"
            };

            var existingCartItem = new CartItem
            {
                BookId = dtoCartItem.BookId,
                UserId = dtoCartItem.UserId,
                Quantity = 1,
                SubtotalPrice = 20.0m,
            };

            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetById(dtoCartItem.BookId)).Returns(existingBook);
            _unitOfWorkMock.Setup(uow => uow.UserRepository.GetById(dtoCartItem.UserId)).Returns(existingUser);
            _unitOfWorkMock.Setup(uow => uow.CartItemRepository.GetCartItemByUserIdAndBookId(dtoCartItem.UserId, dtoCartItem.BookId)).ReturnsAsync(existingCartItem);
            _unitOfWorkMock.Setup(uow => uow.CartItemRepository.Update(It.IsAny<CartItem>()));
            _unitOfWorkMock.Setup(uow => uow.SaveChangeAsync()).ReturnsAsync(0);

            // Act
            var result = await _cartItemService.AddToCart(dtoCartItem);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetById(dtoCartItem.BookId), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.UserRepository.GetById(dtoCartItem.UserId), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveChangeAsync(), Times.Once);

            Assert.Null(result);
        }

        [Fact]
        public async Task ViewCartItems_ReturnsCorrectViewModels()
        {
            // Arrange
            var userId = Guid.NewGuid();
            var pageNumber = 1;
            var pageSize = 10;

            var user = new User();
            var cartItems = new List<CartItem>
            {
                new CartItem { BookId = Guid.NewGuid(), UserId = userId, Quantity = 2 },
                new CartItem { BookId = Guid.NewGuid(), UserId = userId, Quantity = 3 }
            };

            var mappedCartItems = cartItems.Select(cartItem =>
            {
                var viewCartItem = new CartItemVM { Quantity = cartItem.Quantity };
                viewCartItem.BookTitle = "Sample Book Title"; // Replace with your expected book title
                return viewCartItem;
            }).ToList();

            var paginationCartItems = new Pagination<CartItem>
            {
                Items = cartItems,
                TotalItemCount = cartItems.Count,
                PageNumber = pageNumber,
                PageSize = pageSize
            };

            var paginationCartItemsVM = new Pagination<CartItemVM>
            {
                Items = mappedCartItems,
                TotalItemCount = cartItems.Count,
                PageNumber = pageNumber,
                PageSize = pageSize
            };

            _unitOfWorkMock.Setup(uow => uow.UserRepository.GetById(userId)).Returns(user);
            _unitOfWorkMock.Setup(uow => uow.CartItemRepository.Paging(pageNumber, pageSize)).ReturnsAsync(paginationCartItems);
            _unitOfWorkMock.Setup(uow => uow.BookRepository.GetBookTitleById(It.IsAny<Guid>())).Returns("Sample Book Title"); // Replace with your expected book title

            // Act
            var result = await _cartItemService.ViewCartItems(userId, pageNumber, pageSize);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.UserRepository.GetById(userId), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.CartItemRepository.Paging(pageNumber, pageSize), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetBookTitleById(It.IsAny<Guid>()), Times.Exactly(cartItems.Count)); // Ensure it's called for each cart item

            Assert.NotNull(result);
            Assert.Equal(paginationCartItemsVM.Items.Count, result.Items.Count);
        }

        [Fact]
        public async Task ViewCartItems_NoUserFound_ThrowsException()
        {
            // Arrange
            var userId = Guid.NewGuid();
            var pageNumber = 1;
            var pageSize = 10;

            _unitOfWorkMock.Setup(uow => uow.UserRepository.GetById(userId)).Returns((User)null);

            // Act
            var exception = await Assert.ThrowsAsync<Exception>(() => _cartItemService.ViewCartItems(userId, pageNumber, pageSize));

            // Assert
            _unitOfWorkMock.Verify(uow => uow.UserRepository.GetById(userId), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.CartItemRepository.Paging(It.IsAny<int>(), It.IsAny<int>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.BookRepository.GetBookTitleById(It.IsAny<Guid>()), Times.Never);

            Assert.Equal(Constant.UserNotFound, exception.Message);
        }
    }
}

using Application;
using Application.Interfaces;
using Application.Interfaces.IBookServices;
using Application.Interfaces.IuserServices;
using AutoMapper;
using Domain.Commons;
using Domain.Repositories;
using Infrastructure.Mapping;

namespace UnitTest
{
    public class SetupTest
    {
        protected readonly AppConfiguration _appConfiguration;

        protected readonly IMapper _mapperConfig;
        protected readonly Mock<IMapper> _mapperMock;

        protected readonly Mock<IUnitOfWork> _unitOfWorkMock;
        protected readonly Mock<IUserRepository> _userRepositoryMock;
        protected readonly Mock<IBookRepository> _bookRepositoryMock;
        protected readonly Mock<IFavoriteBookRepository> _favoriteBookRepositoryMock;
        protected readonly Mock<IOrderRepository> _orderRepositoryMock;
        protected readonly Mock<IOrderDetailRepository> _orderDetailRepositoryMock;
        protected readonly Mock<IReviewRepository> _reviewRepositoryMock;
        protected readonly Mock<IOTPRepository> _otpRepositoryMock;

        protected readonly Mock<IUserService> _userServiceMock;
        protected readonly Mock<IBookService> _bookServiceMock;
        protected readonly Mock<IFavoriteBookService> _favoriteBookServiceMock;
        protected readonly Mock<IOrderService> _orderServiceMock;
        protected readonly Mock<IOrderDetailService> _orderDetailServiceMock;
        protected readonly Mock<IReviewService> _reviewServiceMock;
        protected readonly Mock<IOTPService> _otpServiceMock;
        protected readonly Mock<ICurrentTimeService> _currentTimeServiceMock;

        public SetupTest()
        {
            _appConfiguration = new AppConfiguration();

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });
            _mapperConfig = mappingConfig.CreateMapper();
            _mapperMock = new Mock<IMapper>();

            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _userRepositoryMock = new Mock<IUserRepository>();
            _bookRepositoryMock = new Mock<IBookRepository>();
            _favoriteBookRepositoryMock = new Mock<IFavoriteBookRepository>();
            _orderRepositoryMock = new Mock<IOrderRepository>();
            _orderDetailServiceMock = new Mock<IOrderDetailService>();
            _reviewRepositoryMock = new Mock<IReviewRepository>();
            _otpRepositoryMock = new Mock<IOTPRepository>();

            _userServiceMock = new Mock<IUserService>();
            _bookServiceMock = new Mock<IBookService>();
            _favoriteBookServiceMock = new Mock<IFavoriteBookService>();
            _orderServiceMock = new Mock<IOrderService>();
            _orderDetailServiceMock = new Mock<IOrderDetailService>();
            _reviewServiceMock = new Mock<IReviewService>();
            _otpServiceMock = new Mock<IOTPService>();
            _currentTimeServiceMock = new Mock<ICurrentTimeService>();
        }
    }
}
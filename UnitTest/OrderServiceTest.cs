﻿using Application.DTOModel;
using Application.Interfaces;
using Application.Services;
using Domain.Commons;
using Domain.Entities;

namespace UnitTest
{
    public class OrderServiceTest : SetupTest
    {
        private readonly IOrderService _orderService;   

        public OrderServiceTest()
        {
            _orderService = new OrderService(_unitOfWorkMock.Object,
                                             _currentTimeServiceMock.Object,
                                             _mapperConfig);
        }

        [Fact]
        public async Task ViewOrders_ReturnsCorrectPagination()
        {
            // Arrange
            var userId = Guid.NewGuid();
            var pageNumber = 1;
            var pageSize = 10;
            var orders = new List<Order> { new Order () };

            _unitOfWorkMock.Setup(uow => uow.OrderRepository.Paging(userId, pageNumber, pageSize)).ReturnsAsync(new Pagination<Order>
            {
                Items = orders,
                TotalItemCount = orders.Count,
                PageNumber = pageNumber,
                PageSize = pageSize
            });

            // Act
            var result = await _orderService.ViewOrders(userId, pageNumber, pageSize);

            // Assert
            _unitOfWorkMock.Verify(uow => uow.OrderRepository.Paging(userId, pageNumber, pageSize), Times.Once);

            Assert.NotNull(result);
            Assert.IsType<Pagination<DTOOrder>>(result);
            Assert.Equal(orders.Count, result.TotalItemCount);
        }
    }
}

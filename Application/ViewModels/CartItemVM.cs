﻿namespace Application.ViewModels
{
    public class CartItemVM
    {
        public string? BookTitle { get; set; }

        public int Quantity { get; set; }

        public decimal SubtotalPrice { get; set; }
    }
}

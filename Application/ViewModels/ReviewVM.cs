﻿namespace Application.ViewModels
{
    public class ReviewVM
    {
        public string? Email { get; set; }

        public string? Image { get; set; }

        public string? Content { get; set; }

        public int Rating { get; set; }
    }
}

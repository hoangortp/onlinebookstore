﻿namespace Application.ViewModels
{
    public class OrderDetailVM
    {
        public string? BookTitle { get; set; }

        public int Quantity { get; set; }

        public decimal SubtotalPrice { get; set; }
    }
}

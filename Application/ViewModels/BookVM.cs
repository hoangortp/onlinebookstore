﻿namespace Presentation.WebAPI.ViewModels
{
    public class BookVM
    {
        public Guid Id { get; set; }

        public string? Title { get; set; }

        public string? Author { get; set; }

        public string? Genre { get; set; }

        public int PublicationYear { get; set; }

        public int StockQuantity { get; set; }

        public decimal Price { get; set; }
    }
}

﻿namespace Application.ViewModels
{
    public class OrderVM
    {
        public Guid Id { get; set; }

        public DateTime PaidDate { get; set; }

        public decimal TotalPrice { get; set; }
    }
}

﻿namespace Application.ViewModels
{
    public class FavoriteBookVM
    {
        public Guid BookId { get; set; }

        public string BookTitle { get; set; }
    }
}

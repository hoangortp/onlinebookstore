﻿namespace Application.ViewModels
{
    public class AccountBalanceVM
    {
        public Guid UserId { get; set; }

        public string? FullName { get; set; }

        public decimal AccountBalance { get; set; }
    }
}

﻿using Application.Interfaces;
using Application.Interfaces.IuserServices;
using Domain.Commons;
using Microsoft.AspNetCore.Hosting;

namespace Application.Services
{
    public class DailyReportService : IDailyReportService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IMailService _mailService;
        private readonly AppConfiguration _appConfiguration;

        public DailyReportService(IUnitOfWork unitOfWork, 
                                  IWebHostEnvironment webHostEnvironment, 
                                  IMailService mailService, 
                                  AppConfiguration appConfiguration)
        {
            _unitOfWork = unitOfWork;
            _webHostEnvironment = webHostEnvironment;
            _mailService = mailService;
            _appConfiguration = appConfiguration;
        }

        public async Task GenerateDailyReport()
        {
            int orderCount = await _unitOfWork.OrderRepository.CountTodayOrder();
            int paidOrderCount = await _unitOfWork.OrderRepository.CountTodayPaidOrder();
            decimal totalIncome = await _unitOfWork.OrderRepository.GetTotalIncome();
            int newAccountCount = await _unitOfWork.UserRepository.CountNewAccount();

            string reportContent = GenerateReportContent(orderCount, paidOrderCount, totalIncome, newAccountCount);

            string reportFileName = "daily_report.pdf";
            string reportFolder = "reports";
            string webRootPath = _webHostEnvironment.WebRootPath;
            string reportPath = Path.Combine(webRootPath, reportFolder, reportFileName);

            await RenderHtmlToPdf(reportContent, reportPath);

            await _mailService.SendReportAsync(_appConfiguration.EmailSection?.Email!, reportPath);
        }

        private static string GenerateReportContent(int orderCount, int paidOrderCount, decimal totalIncome, int newAccountCount)
        {
            string reportContent = $@"
            <html>
            <body>
                <h1>Daily Report</h1>
                <p>Total Orders: {orderCount}</p>
                <p>Total Paid Orders: {paidOrderCount}</p>
                <p>Total Income: ${totalIncome}</p>
                <p>New Accounts Created Today: {newAccountCount}</p>
            </body>
            </html>
        ";

            return reportContent;
        }

        private async Task RenderHtmlToPdf(string content, string filePath)
        {
            var pdf = new ChromePdfRenderer();

            PdfDocument doc = await pdf.RenderHtmlAsPdfAsync(content);

            doc.SaveAs(filePath);
        }
    }
}

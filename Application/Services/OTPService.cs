﻿using Application.Interfaces;
using Application.Interfaces.IuserServices;

namespace Application.Services
{
    public class OTPService : IOTPService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTimeService _currentTimeService;

        public OTPService(IUnitOfWork unitOfWork, ICurrentTimeService currentTimeService)
        {
            _unitOfWork = unitOfWork;
            _currentTimeService = currentTimeService;
        }

        public async Task<bool> VerifyOTP(string email, int enteredOTP, ICurrentTimeService currentTimeService)
        {
            var otp = await _unitOfWork.OTPRepository.GetOTPByEmailAndOTPCode(email, enteredOTP);

            DateTime expirationTime = currentTimeService.GetCurrentTime().AddMinutes(otp.ExpiresInMinutes);

            if (DateTime.Now > expirationTime)
            {
                _unitOfWork.OTPRepository.SoftDelete(otp);

                return false;
            }

            return enteredOTP == otp.OTPCode;
        }
    }
}

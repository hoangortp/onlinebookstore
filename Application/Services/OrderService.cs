﻿using Application.DTOModel;
using Application.Interfaces;
using Application.Utils.Extensions;
using AutoMapper;
using Domain.Commons;
using Domain.Constants;
using Domain.Entities;

namespace Application.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTimeService _currentTimeService;
        private readonly IMapper _mapper;

        public OrderService(IUnitOfWork unitOfWork, ICurrentTimeService currentTimeService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _currentTimeService = currentTimeService;
            _mapper = mapper;
        }

        public async Task<Order?> AddOrder(Guid userId, DTOOrder dtoOrder)
        {
            var user = _unitOfWork.UserRepository.GetById(userId) ?? throw new Exception(Constant.UserNotFound);

            var userCartItems = _unitOfWork.CartItemRepository.ViewCartItems(userId);

            foreach (var item in userCartItems)
            {
                dtoOrder.TotalPrice += item.SubtotalPrice;
            }

            if (user.AccountBalance < dtoOrder.TotalPrice)
            {
                throw new Exception(Constant.NotEnoughMoney);
            }

            var order = dtoOrder.ToOrder();
            order.UserId = userId;
            order.PaidDate = _currentTimeService.GetCurrentTime();

            user.AccountBalance -= order.TotalPrice;

            await _unitOfWork.OrderRepository.AddAsync(order);

            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;

            if (isSuccess)
            {
                return order;
            }

            return null;
        }

        public async Task<Pagination<DTOOrder>> ViewOrders(Guid userId, int pageNumber, int pageSize)
        {
            var orders = await _unitOfWork.OrderRepository.Paging(userId, pageNumber, pageSize);

            Pagination<DTOOrder> dtoOrders = _mapper.Map<Pagination<DTOOrder>>(orders);

            return dtoOrders;
        }
    }
}

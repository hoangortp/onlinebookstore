﻿using Application.DTOModel.DTOBookModels;
using Application.Interfaces.IBookServices;
using Application.Utils;
using Application.Utils.Extensions;
using AutoMapper;
using Domain.Commons;
using Domain.Constants;
using Presentation.WebAPI.ViewModels;

namespace Application.Services.BookServices
{
    public class BookService : IBookService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public BookService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public DTOBook GetBookById(Guid bookId)
        {
            var book = _unitOfWork.BookRepository.GetById(bookId) ?? throw new Exception(Constant.BookNotFound);

            return book.ToDTOBook();
        }

        public async Task<Pagination<DTOBook>> GetBooks(int pageNumber, int pageSize)
        {
            var books = await _unitOfWork.BookRepository.Paging(pageNumber, pageSize);

            var dtoBooks = _mapper.Map<Pagination<DTOBook>>(books);

            return dtoBooks;
        }

        public async Task<DTOBook?> AddBook(DTOCreatedBook dtoCreateBook)
        {
            var book = dtoCreateBook.ToBook();
            var isValid = book.IsValidGenre();

            if (isValid == false)
            {
                throw new Exception(Constant.GenreInvalid);
            }

            await _unitOfWork.BookRepository.AddAsync(book);

            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;

            if (isSuccess)
            {
                return book.ToDTOBook();
            }

            return null;
        }

        public async Task<List<DTOBook>?> AddRangeBook(List<DTOCreatedBook> dtoCreateBooks)
        {
            var books = dtoCreateBooks.ToListBook();
            var isValid = books.AreValidGenre();

            if (isValid == false)
            {
                throw new Exception(Constant.GenreInvalid);
            }

            await _unitOfWork.BookRepository.AddRangeAsync(books);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;

            if (isSuccess)
            {
                return books.ToListDTOBook();
            }

            return null;
        }

        public async Task<DTOBook?> SoftDeleteBook(Guid bookId)
        {
            var deletedBook = _unitOfWork.BookRepository.GetById(bookId) ?? throw new Exception(Constant.BookNotFound);

            _unitOfWork.BookRepository.SoftDelete(deletedBook);

            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;

            if (isSuccess)
            {
                return deletedBook.ToDTOBook();
            }

            return null;
        }

        public async Task<List<DTOBook>?> SoftDeleteRangeBook(List<Guid> id)
        {
            var books = await _unitOfWork.BookRepository.GetAllAsync();

            var deletedBooks = books.Where(x => id.Contains(x.Id)).ToList() ?? throw new Exception(Constant.BookNotFound);

            _unitOfWork.BookRepository.SoftDeleteRange(deletedBooks);

            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;

            if (isSuccess)
            {
                return deletedBooks.ToListDTOBook();
            }

            return null;
        }

        public async Task<DTOBook?> UpdateBook(DTOBook dtoBook)
        {
            var updatedBook = _unitOfWork.BookRepository.GetById(dtoBook.Id) ?? throw new Exception(Constant.BookNotFound);

            updatedBook.Title = dtoBook.Title;
            updatedBook.Author = dtoBook.Author;
            updatedBook.Genre = dtoBook.Genre;
            updatedBook.PublicationYear = dtoBook.PublicationYear;
            updatedBook.StockQuantity = dtoBook.StockQuantity;
            updatedBook.Price = dtoBook.Price;

            _unitOfWork.BookRepository.Update(updatedBook);

            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;

            if (isSuccess)
            {
                return updatedBook.ToDTOBook();
            }

            return null;
        }
    }
}

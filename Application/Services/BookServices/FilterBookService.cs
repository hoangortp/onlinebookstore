﻿using Application.DTOModel.DTOBookModels;
using Application.Interfaces.IBookServices;
using AutoMapper;
using Domain.Commons;

namespace Application.Services.BookServices
{
    public class FilterBookService : IFilterBookService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public FilterBookService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Pagination<DTOBook>> FilterByTitleAsync(string bookTitle, int pageNumber, int pageSize)
        {
            var filter = await _unitOfWork.BookRepository.Filter(x => x.Title!.Contains(bookTitle), pageNumber, pageSize);

            Pagination<DTOBook> filteredDTOBooks = _mapper.Map<Pagination<DTOBook>>(filter);

            return filteredDTOBooks;
        }

        public async Task<Pagination<DTOBook>> FilterByAuthorAsync(string author, int pageNumber, int pageSize)
        {
            var filter = await _unitOfWork.BookRepository.Filter(x => x.Author!.Contains(author), pageNumber, pageSize);

            Pagination<DTOBook> filteredDTOBooks = _mapper.Map<Pagination<DTOBook>>(filter);

            return filteredDTOBooks;
        }

        public async Task<Pagination<DTOBook>> FilterByYearAsync(int year, int pageNumber, int pageSize)
        {
            var filter = await _unitOfWork.BookRepository.Filter(x => x.PublicationYear == year, pageNumber, pageSize);

            Pagination<DTOBook> filteredDTOBooks = _mapper.Map<Pagination<DTOBook>>(filter);

            return filteredDTOBooks;

        }

        public async Task<Pagination<DTOBook>> FilterByGenreAsync(string genre, int pageNumber, int pageSize)
        {
            var filter = await _unitOfWork.BookRepository.Filter(x => x.Genre!.Contains(genre), pageNumber, pageSize);

            Pagination<DTOBook> filteredDTOBooks = _mapper.Map<Pagination<DTOBook>>(filter);

            return filteredDTOBooks;

        }
    }
}

﻿using Application.DTOModel;
using Application.Interfaces;
using Application.Utils.Extensions;
using Application.ViewModels;
using AutoMapper;
using Domain.Commons;
using Domain.Constants;
using Domain.Entities;

namespace Application.Services
{
    public class OrderDetailService : IOrderDetailService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public OrderDetailService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddOrderDetail(Guid userId, Order order)
        {
            _ = _unitOfWork.UserRepository.GetById(userId) ?? throw new Exception(Constant.UserNotFound);
            var cartItems = _unitOfWork.CartItemRepository.ViewCartItems(userId);

            foreach (var item in cartItems)
            {
                DTOOrderDetail dtoOrderDetail = new DTOOrderDetail
                {
                    OrderId = order.Id,
                    BookId = item.BookId,
                    Quantity = item.Quantity,
                    SubtotalPrice = item.SubtotalPrice,
                };

                var orderDetail = dtoOrderDetail.ToOrderDetail();

                await _unitOfWork.OrderDetailRepository.AddAsync(orderDetail);
            }

            _unitOfWork.CartItemRepository.SoftDeleteRange(cartItems);

            await _unitOfWork.SaveChangeAsync();
        }

        public async Task<Pagination<OrderDetailVM>> ViewOrderDetails(Guid orderId, int pageNumber, int pageSize)
        {
            var orderDetails = await _unitOfWork.OrderDetailRepository.Paging(orderId, pageNumber, pageSize);

            var mappedOrderDetails = orderDetails?.Items?.Select(orderDetail =>
            {
                var viewOrderDetail = orderDetail.ToOrderDetailViewModel();

                viewOrderDetail.BookTitle = _unitOfWork.BookRepository.GetBookTitleById(orderDetail.BookId);

                return viewOrderDetail;
            }).ToList();

            Pagination<OrderDetailVM> viewOrderDetails = new()
            {
                Items = mappedOrderDetails,
                TotalItemCount = orderDetails!.TotalItemCount,
                PageSize = pageSize,
                PageNumber = pageNumber
            };

            return viewOrderDetails;
        }
    }
}

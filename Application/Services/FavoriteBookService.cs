﻿using Application.DTOModel;
using Application.Interfaces;
using Application.Utils.Extensions;
using Application.ViewModels;
using Domain.Commons;
using Domain.Constants;
using Domain.Entities;

namespace Application.Services
{
    public class FavoriteBookService : IFavoriteBookService
    {
        private readonly IUnitOfWork _unitOfWork;

        public FavoriteBookService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<DTOFavoriteBook?> AddFavoriteBook(DTOFavoriteBook dtoFavoriteBook)
        {
            var book = _unitOfWork.BookRepository.GetById(dtoFavoriteBook.BookId) ?? throw new Exception(Constant.BookNotFound);
            var user = _unitOfWork.UserRepository.GetById(dtoFavoriteBook.UserId) ?? throw new Exception(Constant.UserNotFound);

            var existingFavorite = await _unitOfWork.FavoriteBookRepository.GetFavoriteBookByUserIdAndBookId(user.Id, book.Id);

            if (existingFavorite is not null)
            {
                existingFavorite.IsDeleted = false;

                _unitOfWork.FavoriteBookRepository.Update(existingFavorite);
            }
            else
            {
                FavoriteBook favoriteBook = dtoFavoriteBook.ToFavoriteBook();

                await _unitOfWork.FavoriteBookRepository.AddAsync(favoriteBook);
            }

            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;

            if (isSuccess)
            {
                return dtoFavoriteBook;
            }

            return null;
        }

        public async Task<Pagination<FavoriteBookVM>> ViewFavoriteBooks(Guid userId, int pageNumber, int pageSize)
        {
            var user = _unitOfWork.UserRepository.GetById(userId) ?? throw new Exception(Constant.UserNotFound);

            var favoriteBooks = await _unitOfWork.FavoriteBookRepository.Paging(pageNumber, pageSize);

            var mappedFavoriteBook = favoriteBooks?.Items?.Select(favoriteBook =>
            {
                var viewFavoriteBook = favoriteBook.ToFavoriteBookViewModel();

                viewFavoriteBook.BookTitle = _unitOfWork.BookRepository.GetBookTitleById(favoriteBook.BookId);

                return viewFavoriteBook;
            }).ToList();

            Pagination<FavoriteBookVM> viewFavoriteBooks = new Pagination<FavoriteBookVM>
            {
                Items = mappedFavoriteBook,
                TotalItemCount = favoriteBooks!.TotalItemCount,
                PageNumber = pageNumber,
                PageSize = pageSize
            };

            return viewFavoriteBooks;
        }

        public async Task<DTOFavoriteBook?> RemoveFavoriteBook(DTOFavoriteBook dtoFavoriteBook)
        {
            var existingFavoriteBook = await _unitOfWork.FavoriteBookRepository
                                                        .GetFavoriteBookByUserIdAndBookId(dtoFavoriteBook.UserId, dtoFavoriteBook.BookId)
                                                        ?? throw new Exception(Constant.BookNotFound);

            _unitOfWork.FavoriteBookRepository.SoftDelete(existingFavoriteBook);

            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;

            if (isSuccess)
            {
                return dtoFavoriteBook;
            }

            return null;
        }
    }
}

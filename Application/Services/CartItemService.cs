﻿using Application.DTOModel;
using Application.Interfaces;
using Application.Utils.Extensions;
using Application.ViewModels;
using AutoMapper;
using Domain.Commons;
using Domain.Constants;
using Domain.Entities;

namespace Application.Services.CartItemServices
{
    public class CartItemService : ICartItemService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CartItemService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<DTOCartItem?> AddToCart(DTOCartItem dtoCartItem)
        {
            var book = _unitOfWork.BookRepository.GetById(dtoCartItem.BookId) ?? throw new Exception(Constant.BookNotFound);
            var user = _unitOfWork.UserRepository.GetById(dtoCartItem.UserId) ?? throw new Exception(Constant.UserNotFound);

            if (book.StockQuantity < dtoCartItem.Quantity || book.StockQuantity == 0)
            {
                throw new Exception(Constant.CannotHigherThanStock);
            }

            var existingCartItem = await _unitOfWork.CartItemRepository.GetCartItemByUserIdAndBookId(user.Id, book.Id);

            if (existingCartItem.Quantity + dtoCartItem.Quantity > book.StockQuantity)
            {
                throw new Exception(Constant.CannotHigherThanStock);
            }

            if (existingCartItem is not null)
            {
                existingCartItem.Quantity += dtoCartItem.Quantity;
                dtoCartItem.SubtotalPrice = book.Price * dtoCartItem.Quantity;
                existingCartItem.SubtotalPrice += dtoCartItem.SubtotalPrice;
            }
            else
            {
                dtoCartItem.SubtotalPrice = book.Price * dtoCartItem.Quantity;

                CartItem cartItem = _mapper.Map<CartItem>(dtoCartItem);

                await _unitOfWork.CartItemRepository.AddAsync(cartItem);
            }

            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;

            if (isSuccess)
            {
                return dtoCartItem;
            }

            return null;
        }

        public async Task<Pagination<CartItemVM>> ViewCartItems(Guid userId, int pageNumber, int pageSize)
        {
            var user = _unitOfWork.UserRepository.GetById(userId) ?? throw new Exception(Constant.UserNotFound);

            var cartItems = await _unitOfWork.CartItemRepository.Paging(pageNumber, pageSize);

            var mappedCartItems = cartItems.Items!.Select(cartItem =>
            {
                var viewCartItem = cartItem.ToCartItemViewModel();

                viewCartItem.BookTitle = _unitOfWork.BookRepository.GetBookTitleById(cartItem.BookId);

                return viewCartItem;
            }).ToList();

            Pagination<CartItemVM> viewCartItems = new Pagination<CartItemVM>
            {
                Items = mappedCartItems,
                TotalItemCount = cartItems.TotalItemCount,
                PageNumber = pageNumber,
                PageSize = pageSize,
            };

            return viewCartItems;
        }

        public async Task<DTOCartItem?> RemoveFromCart(DTOCartItem cartItem)
        {
            var book = _unitOfWork.BookRepository.GetById(cartItem.BookId) ?? throw new Exception(Constant.BookNotFound);
            var existingCartItem = await _unitOfWork.CartItemRepository.GetCartItemByUserIdAndBookId(cartItem.UserId, cartItem.BookId);

            if (existingCartItem is null)
            {
                return null;
            }

            if (cartItem.Quantity > existingCartItem.Quantity)
            {
                throw new Exception(Constant.CannotHigherThanCart);
            }

            if (cartItem.Quantity == existingCartItem.Quantity)
            {
                _unitOfWork.CartItemRepository.SoftDelete(existingCartItem);
            }

            existingCartItem.Quantity -= cartItem.Quantity;
            existingCartItem.SubtotalPrice -= cartItem.Quantity * book.Price;

            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;

            if (isSuccess)
            {
                return cartItem;
            }

            return null;
        }
    }
}

﻿using Application.DTOModel;
using Application.Interfaces;
using Application.Utils.Extensions;
using Application.ViewModels;
using AutoMapper;
using Domain.Commons;
using Domain.Constants;

namespace Application.Services
{
    public class ReviewService : IReviewService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ReviewService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<DTOReview?> AddReview(Guid userId, DTOReview dtoReview)
        {
            _ = _unitOfWork.UserRepository.GetById(userId) ?? throw new Exception(Constant.UserNotFound);

            var review = dtoReview.ToReview();

            await _unitOfWork.ReviewRepository.AddAsync(review);

            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;

            if (isSuccess)
            {
                return dtoReview;
            }

            return null;
        }

        public async Task<Pagination<ReviewVM>> ViewReviews(Guid bookId, int pageNumber, int pageSize)
        {
            _ = _unitOfWork.BookRepository.GetById(bookId) ?? throw new Exception(Constant.BookNotFound);

            var reviews = await _unitOfWork.ReviewRepository.Paging(bookId,pageNumber, pageSize);

            Pagination<ReviewVM> viewReviews = _mapper.Map<Pagination<ReviewVM>>(reviews);

            return viewReviews;
        } 
    }
}

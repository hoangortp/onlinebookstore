﻿using Application.DTOModel.DTOUser;
using Application.DTOModel.DTOUserModels;
using Application.Interfaces;
using Application.Interfaces.IuserServices;
using Application.Utils.AuthUtils;
using Application.Utils.Extensions;
using AutoMapper;
using Domain.Commons;
using Domain.Constants;
using Domain.Entities;

namespace Application.Services.UserServices
{
    public class AuthService : IAuthService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTimeService _currentTimeService;
        private readonly IOTPService _otpService;
        private readonly AppConfiguration _appConfiguration;

        public AuthService(IUnitOfWork unitOfWork,
            IMapper mapperConfig,
            ICurrentTimeService currentTimeService,
            AppConfiguration appConfiguration,
            IOTPService otpService)
        {
            _unitOfWork = unitOfWork;
            _currentTimeService = currentTimeService;
            _appConfiguration = appConfiguration;
            _otpService = otpService;
        }

        public async Task<DTOUserRegister?> RegisterAsync(DTOUserRegister dtoUserRegister)
        {
            var isExisted = await _unitOfWork.UserRepository.CheckEmailExisted(dtoUserRegister.Email!);

            if (isExisted)
            {
                throw new Exception(Constant.EmailExists);
            }

            string salt = PasswordUtils.Salt();

            var newUser = new User
            {
                FullName = dtoUserRegister.FullName,
                Email = dtoUserRegister.Email,
                Role = dtoUserRegister.Role,
                Salt = salt,
                HashedPassword = PasswordUtils.Hash(string.Concat(dtoUserRegister.Password, salt))
            };

            await _unitOfWork.UserRepository.AddAsync(newUser);

            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;

            if (isSuccess)
            {
                return newUser.ToDTOUserRegister();
            }

            return null;
        }

        public async Task<DTOAdminRegister?> RegisterAdminAsync(DTOAdminRegister dtoAdminRegister)
        {
            var isExisted = await _unitOfWork.UserRepository.CheckEmailExisted(dtoAdminRegister.Email!);

            if (isExisted)
            {
                throw new Exception(Constant.EmailExists);
            }

            var adminExists = await _unitOfWork.UserRepository.CheckAdminExists();

            if (adminExists)
            {
                throw new Exception(Constant.MoreThanOneAdmin);
            }

            string salt = PasswordUtils.Salt();

            var newUser = new User
            {
                FullName = dtoAdminRegister.FullName,
                Email = dtoAdminRegister.Email,
                Role = dtoAdminRegister.Role,
                Salt = salt,
                HashedPassword = PasswordUtils.Hash(string.Concat(dtoAdminRegister.Password, salt))
            };

            await _unitOfWork.UserRepository.AddAsync(newUser);

            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;

            if (isSuccess)
            {
                return newUser.ToDTOAdminRegister();
            }

            return null;
        }

        public async Task<string> LoginAsync(DTOUserLogin dtoUserLogin)
        {
            var user = await _unitOfWork.UserRepository.GetUserBasedOnEmail(dtoUserLogin.Email!);
            string hashedPasswordSalt = PasswordUtils.HashWithSalt(dtoUserLogin.Password!, user.Salt!);
            var loginUser = await _unitOfWork.UserRepository.GetEmailAndHashedPassword(dtoUserLogin.Email!, hashedPasswordSalt);

            var dtoLoginUser = loginUser.ToDTOUserLogin();

            return dtoLoginUser.GenerateWebJsonToken(_currentTimeService.GetCurrentTime(), _appConfiguration);
        }

        public async Task<DTOUserResetPassword?> ResetPassword(DTOUserResetPassword dtoUserResetPassword)
        {
            var user = await _unitOfWork.UserRepository.GetUserBasedOnEmail(dtoUserResetPassword.Email!);
            string hashedPasswordSalt = PasswordUtils.HashWithSalt(dtoUserResetPassword.NewPassword!, user.Salt!);
            var isCorrect = await _otpService.VerifyOTP(dtoUserResetPassword.Email!, dtoUserResetPassword.OTP, _currentTimeService);

            if (isCorrect)
            {
                DTOUserResetPassword userResetPassword = new()
                {
                    Email = dtoUserResetPassword.Email,
                    OTP = dtoUserResetPassword.OTP,
                    NewPassword = hashedPasswordSalt
                };

                user.HashedPassword = userResetPassword.NewPassword;

                _unitOfWork.UserRepository.Update(user);

                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;

                if (isSuccess)
                {
                    return userResetPassword;
                }
            }

            return null;
        }
    }
}

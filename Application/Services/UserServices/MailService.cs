﻿using Application.Interfaces.IuserServices;
using Application.Utils.AuthUtils;
using Domain.Constants;
using Domain.Entities;
using System.Net;
using System.Net.Mail;
using Domain.Commons;

namespace Application.Services.UserServices
{
    public class MailService : IMailService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly AppConfiguration _appConfiguration;

        public MailService(IUnitOfWork unitOfWork, AppConfiguration appConfiguration)
        {
            _unitOfWork = unitOfWork;
            _appConfiguration = appConfiguration;
        }

        public async Task SendOTPMailAsync(string email)
        {
            string emailForSend = _appConfiguration.EmailSection?.Email!;
            string appPasswordConfiguration = _appConfiguration.EmailSection?.AppPassword!;

            var newOTP = OTPUtils.GenerateOTP();
            var isEmailExists = await _unitOfWork.UserRepository.CheckEmailExisted(email);

            if (isEmailExists == false)
            {
                throw new Exception(Constant.EmailNotFound);
            }

            OTP otp = new()
            {
                Email = email,
                OTPCode = newOTP,
            };

            await _unitOfWork.OTPRepository.AddAsync(otp);
            await _unitOfWork.SaveChangeAsync();

            var smtpClient = new SmtpClient
            {
                Port = 587,
                EnableSsl = true,
                Host = "smtp.gmail.com",
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(emailForSend, appPasswordConfiguration),
            };

            var message = new MailMessage()
            {
                Subject = "subject",
                Body = newOTP.ToString(),
                From = new MailAddress(emailForSend),
            };

            message.To.Add(new MailAddress(email));

            await smtpClient.SendMailAsync(message);
        }

        public async Task SendReportAsync(string email, string path)
        {
            string emailForSend = _appConfiguration.EmailSection?.Email!;
            string appPasswordConfiguration = _appConfiguration.EmailSection?.AppPassword!;

            var isEmailExists = await _unitOfWork.UserRepository.CheckEmailExisted(email);

            if (isEmailExists == false)
            {
                throw new Exception(Constant.EmailNotFound);
            }

            var smtpClient = new SmtpClient
            {
                Port = 587,
                EnableSsl = true,
                Host = "smtp.gmail.com",
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(emailForSend, appPasswordConfiguration),
            };

            var message = new MailMessage()
            {
                Subject = _appConfiguration.EmailSection?.ReportSubject,
                Body = _appConfiguration.EmailSection?.ReportBody,
                From = new MailAddress(emailForSend!),
            };

            message.To.Add(new MailAddress(email));

            Attachment attachment = new(path, System.Net.Mime.MediaTypeNames.Application.Pdf);
            
            message.Attachments.Add(attachment);

            await smtpClient.SendMailAsync(message);
        }
    }
}

﻿using Application.DTOModel.DTOUser;
using Application.Interfaces.IuserServices;
using Application.Utils.Extensions;
using Application.ViewModels;
using Domain.Constants;
using Domain.Entities;

namespace Application.Services.UserServices
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<User> GetUserByEmail(string email)
        {
            var user = await _unitOfWork.UserRepository.GetUserBasedOnEmail(email) ?? throw new Exception(Constant.UserNotFound);

            return user;
        }

        public async Task<DTOUserUpdate?> UpdateUser(DTOUserUpdate dtoUserUpdate)
        {
            var user = _unitOfWork.UserRepository.GetById(dtoUserUpdate.Id) ?? throw new Exception(Constant.UserNotFound);

            user.FullName = dtoUserUpdate.FullName;

            _unitOfWork.UserRepository.Update(user);

            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;

            if (isSuccess)
            {
                return user.ToDTOUserUpdate();
            }

            return null;
        }

        public async Task ChargeMoney(DTOUserChargeMoney chargeMoney)
        {
            var user = _unitOfWork.UserRepository.GetById(chargeMoney.UserId);

            if (chargeMoney.Money < 1)
            {
                throw new Exception(Constant.LowerThanOne);
            }

            user.AccountBalance += chargeMoney.Money;

            await _unitOfWork.SaveChangeAsync();
        }

        public AccountBalanceVM ViewAccountBalance(Guid userId)
        {
            var user = _unitOfWork.UserRepository.GetById(userId) ?? throw new Exception(Constant.UserNotFound);

            return user.ToAccountBalanceVM();
        }
    }
}

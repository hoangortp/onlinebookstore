﻿namespace Application.DTOModel
{
    public class DTOCartItem
    {
        public Guid UserId { get; set; }
        public Guid BookId { get; set; }
        public int Quantity { get; set; }
        public decimal SubtotalPrice { get; set; }
    }
}

﻿namespace Application.DTOModel
{
    public class DTOFavoriteBook
    {
        public Guid UserId { get; set; }
        public Guid BookId { get; set; }
    }
}

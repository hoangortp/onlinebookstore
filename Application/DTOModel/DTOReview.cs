﻿namespace Application.DTOModel
{
    public class DTOReview
    {
        public Guid BookId { get; set; }
        public Guid UserId { get; set; }
        public string? Image { get; set; }
        public string? Content { get; set; }
        public int Rating { get; set; }
    }
}

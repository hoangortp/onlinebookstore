﻿namespace Application.DTOModel
{
    public class DTOOrder
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public DateTime PaidDate { get; set; }
        public decimal TotalPrice { get; set; }
    }
}

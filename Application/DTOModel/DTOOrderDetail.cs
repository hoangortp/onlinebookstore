﻿namespace Application.DTOModel
{
    public class DTOOrderDetail
    {
        public Guid OrderId { get; set; }
        public Guid BookId { get; set; }
        public int Quantity { get; set; }
        public decimal SubtotalPrice { get; set; }
    }
}

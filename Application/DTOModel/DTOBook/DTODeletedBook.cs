﻿namespace Application.DTOModel.DTOBook
{
    public class DTODeletedBook
    {
        public Guid Id { get; set; }
    }
}

﻿namespace Application.DTOModel.DTOUser
{
    public class DTOAdminRegister
    {
        public string? FullName { get; set; }
        public string? Email { get; set; }
        public string? Role { get; set; } = "admin";
        public string? Password { get; set; }
    }
}

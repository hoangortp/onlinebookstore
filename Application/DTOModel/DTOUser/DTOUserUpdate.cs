﻿namespace Application.DTOModel.DTOUser
{
    public class DTOUserUpdate
    {
        public Guid Id { get; set; }
        public string? FullName { get; set; }
    }
}

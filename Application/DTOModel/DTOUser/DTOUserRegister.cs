﻿namespace Application.DTOModel.DTOUserModels
{
    public class DTOUserRegister
    {
        public string? FullName { get; set; }
        public string? Email { get; set; }
        public string? Role { get; set; } = "customer";
        public string? Password { get; set; }
    }
}

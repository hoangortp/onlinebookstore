﻿namespace Application.DTOModel.DTOUserModels
{
    public class DTOUserLogin
    {
        public string? Email { get; set; }
        public string? Password { get; set; }
        public string? Role { get; set; }   
    }
}

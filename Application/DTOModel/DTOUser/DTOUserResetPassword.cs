﻿namespace Application.DTOModel.DTOUserModels
{
    public class DTOUserResetPassword
    {
        public string? Email { get; set; }
        public int OTP { get; set; }
        public string? NewPassword { get; set; }
    }
}

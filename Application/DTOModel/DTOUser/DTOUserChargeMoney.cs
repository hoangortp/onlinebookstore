﻿namespace Application.DTOModel.DTOUser
{
    public class DTOUserChargeMoney
    {
        public Guid UserId { get; set; }
        public decimal Money { get; set; }
    }
}

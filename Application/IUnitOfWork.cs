﻿using Domain.Repositories;

namespace Application
{
    public interface IUnitOfWork
    {
        public IBookRepository BookRepository { get; }
        public IUserRepository UserRepository { get; }
        public IOrderRepository OrderRepository { get; }
        public IOrderDetailRepository OrderDetailRepository { get; }
        public IReviewRepository ReviewRepository { get; }
        public ICartItemRepository CartItemRepository { get; }
        public IFavoriteBookRepository FavoriteBookRepository { get; }
        public IOTPRepository OTPRepository { get; }
        public Task<int> SaveChangeAsync();
    }
}

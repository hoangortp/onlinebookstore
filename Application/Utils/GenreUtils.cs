﻿using Domain.Entities;
using Domain.Enums;

namespace Application.Utils
{
    public static class GenreUtils
    {
        public static bool IsValidGenre(this Book book)
        {
            return Enum.GetNames(typeof(Genre)).Contains(book.Genre);
        }

        public static bool AreValidGenre(this ICollection<Book> books)
        {
            return books.All(book => book.IsValidGenre());
        }
    }
}   

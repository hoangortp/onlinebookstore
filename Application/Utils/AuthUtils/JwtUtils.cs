﻿using Application.DTOModel.DTOUserModels;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Domain.Commons;

namespace Application.Utils.AuthUtils
{
    public static class JwtUtils
    {
        public static string GenerateWebJsonToken(this DTOUserLogin dtoUserLogin, DateTime now, AppConfiguration appConfiguration)
        {
            var secretInBytes = Encoding.UTF8.GetBytes(appConfiguration.JWTSection!.SecretKey!);
            var securityKey = new SymmetricSecurityKey(secretInBytes);
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, dtoUserLogin.Email!),
                new Claim(ClaimTypes.Role, dtoUserLogin.Role!),
            };

            var token = new JwtSecurityToken(
                issuer: appConfiguration.JWTSection.Issuer,
                audience: appConfiguration.JWTSection.Audience,
                expires: now.AddMinutes(appConfiguration.JWTSection.ExpiresInMinutes),
                claims: claims,
                signingCredentials: credentials
            );

            var jwtTokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = jwtTokenHandler.WriteToken(token);
            return jwtToken;
        }
    }
}

﻿using System.Security.Cryptography;
using System.Text;

namespace Application.Utils.AuthUtils
{
    public static class PasswordUtils
    {
        public static string Salt()
        {
            byte[] bytes = RandomNumberGenerator.GetBytes(3);
            string salt = Convert.ToBase64String(bytes);

            return salt;
        }

        public static string Hash(this string input)
        {
            using SHA256 sha256 = SHA256.Create();

            var inputBytes = Encoding.UTF8.GetBytes(input);
            byte[] bytes = sha256.ComputeHash(inputBytes);

            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < bytes.Length; i++)
            {
                builder.Append(bytes[i].ToString("x2"));
            }

            return builder.ToString();
        }

        public static string HashWithSalt(this string password, string salt)
        {
            string saltedPassword = password + salt;

            return saltedPassword.Hash();
        }
    }
}

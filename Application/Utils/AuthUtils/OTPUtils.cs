﻿namespace Application.Utils.AuthUtils
{
    public static class OTPUtils
    {
        public static int GenerateOTP()
        {
            Random random = new Random();
            int otpCode = random.Next(100000, 999999);

            return otpCode;
        }
    }
}

﻿using Application.DTOModel;
using Application.ViewModels;
using Domain.Entities;

namespace Application.Utils.Extensions
{
    public static class FavoriteBookExtension
    {
        public static DTOFavoriteBook ToDTOFavoriteBook(this FavoriteBook favoriteBook)
        {
            DTOFavoriteBook dtoFavoriteBook = new DTOFavoriteBook
            {
                UserId = favoriteBook.UserId,
                BookId = favoriteBook.BookId,
            };

            return dtoFavoriteBook;
        }

        public static FavoriteBook ToFavoriteBook(this DTOFavoriteBook dtoFavoriteBook)
        {
            FavoriteBook favoriteBook = new FavoriteBook
            {
                UserId = dtoFavoriteBook.UserId,
                BookId = dtoFavoriteBook.BookId,
            };

            return favoriteBook;
        }

        public static FavoriteBookVM ToFavoriteBookViewModel(this FavoriteBook favoriteBook)
        {
            FavoriteBookVM viewFavoriteBook = new FavoriteBookVM
            {
                BookId = favoriteBook.BookId
            };

            return viewFavoriteBook;
        }
    }
}

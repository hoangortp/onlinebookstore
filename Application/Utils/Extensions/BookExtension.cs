﻿using Application.DTOModel.DTOBookModels;
using Domain.Entities;
using Presentation.WebAPI.ViewModels;

namespace Application.Utils.Extensions
{
    public static class BookExtension
    {
        public static DTOBook ToDTOBook(this Book book)
        {
            return new DTOBook
            {
                Id = book.Id,
                Title = book.Title,
                Author = book.Author,
                Genre = book.Genre,
                PublicationYear = book.PublicationYear,
                StockQuantity = book.StockQuantity,
                Price = book.Price,
            };
        }

        public static List<DTOBook> ToListDTOBook(this List<Book> books)
        {
            return books.Select(book => new DTOBook
            {
                Id = book.Id,
                Title = book.Title,
                Author = book.Author,
                Genre = book.Genre,
                PublicationYear = book.PublicationYear,
                StockQuantity = book.StockQuantity,
                Price = book.Price,
            }).ToList();
        }

        public static Book ToBook(this DTOCreatedBook createBook)
        {
            return new Book
            {
                Title = createBook.Title,
                Author = createBook.Author,
                Genre = createBook.Genre,
                PublicationYear = createBook.PublicationYear,
                StockQuantity = createBook.StockQuantity,
                Price = createBook.Price,
            };
        }

        public static List<Book> ToListBook(this List<DTOCreatedBook> createBooks)
        {
            return createBooks.Select(createBook => new Book
            {
                Title = createBook.Title,
                Author = createBook.Author,
                Genre = createBook.Genre,
                PublicationYear = createBook.PublicationYear,
                StockQuantity = createBook.StockQuantity,
                Price = createBook.Price,
            }).ToList();
        }

        public static BookVM ToCustomerBookViewModel(this DTOBook dTOBook)
        {
            BookVM customerBookViewModel = new BookVM
            {
                Id = dTOBook.Id,
                Title = dTOBook.Title,
                Author = dTOBook.Author,
                Genre = dTOBook.Genre,
                PublicationYear = dTOBook.PublicationYear,
                StockQuantity = dTOBook.StockQuantity,
                Price = dTOBook.Price,
            };

            return customerBookViewModel;
        }
    }
}

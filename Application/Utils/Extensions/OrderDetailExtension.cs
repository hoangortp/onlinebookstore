﻿using Application.DTOModel;
using Application.ViewModels;
using Domain.Entities;

namespace Application.Utils.Extensions
{
    public static class OrderDetailExtension
    {
        public static OrderDetail ToOrderDetail(this DTOOrderDetail dTOOrderDetail)
        {
            return new OrderDetail
            {
                OrderId = dTOOrderDetail.OrderId,
                BookId = dTOOrderDetail.BookId,
                Quantity = dTOOrderDetail.Quantity,
                SubtotalPrice = dTOOrderDetail.SubtotalPrice,
            };
        }

        public static OrderDetailVM ToOrderDetailViewModel(this OrderDetail orderDetail)
        {
            return new OrderDetailVM
            {
                Quantity = orderDetail.Quantity,
                SubtotalPrice = orderDetail.SubtotalPrice
            };
        }
    }
}

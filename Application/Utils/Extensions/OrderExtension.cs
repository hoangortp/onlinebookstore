﻿using Application.DTOModel;
using Domain.Entities;

namespace Application.Utils.Extensions
{
    public static class OrderExtension
    {
        public static Order ToOrder(this DTOOrder dtoOrder)
        {
            return new Order
            {
                UserId = dtoOrder.UserId,
                PaidDate = dtoOrder.PaidDate,
                TotalPrice = dtoOrder.TotalPrice,
            };
        }
    }
}

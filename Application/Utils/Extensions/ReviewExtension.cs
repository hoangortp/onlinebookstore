﻿using Application.DTOModel;
using Domain.Entities;

namespace Application.Utils.Extensions
{
    public static class ReviewExtension
    {
        public static Review ToReview(this DTOReview dtoReview)
        {
            return new Review
            {
                BookId = dtoReview.BookId,
                UserId = dtoReview.UserId,
                Image = dtoReview.Image,
                Content = dtoReview.Content,
                Rating = dtoReview.Rating,
            };
        }
    }
}

﻿using Application.DTOModel.DTOUser;
using Application.DTOModel.DTOUserModels;
using Application.ViewModels;
using Domain.Entities;

namespace Application.Utils.Extensions
{
    public static class UserExtension
    {
        public static DTOUserLogin ToDTOUserLogin(this User user)
        {
            return new DTOUserLogin
            {
                Email = user.Email,
                Password = user.HashedPassword,
                Role = user.Role,
            };
        }

        public static DTOUserRegister ToDTOUserRegister(this User user)
        {
            return new DTOUserRegister
            {
                FullName = user.FullName,
                Email = user.Email,
                Password = user.HashedPassword,
                Role = user.Role,
            };
        }

        public static DTOAdminRegister ToDTOAdminRegister(this User user)
        {
            return new DTOAdminRegister
            {
                FullName = user.FullName,
                Email = user.Email,
                Password = user.HashedPassword,
                Role = user.Role,
            };
        }

        public static DTOUserResetPassword ToDTOUserResetPassword(this User user, OTP otp)
        {
            return new DTOUserResetPassword
            {
                Email = user.Email,
                OTP = otp.OTPCode,
                NewPassword = user.HashedPassword
            };
        }

        public static AccountBalanceVM ToAccountBalanceVM(this User user)
        {
            return new AccountBalanceVM
            {
                UserId = user.Id,
                FullName = user.FullName,
                AccountBalance = user.AccountBalance
            };
        }

        public static DTOUserUpdate ToDTOUserUpdate(this User user)
        {
            return new DTOUserUpdate
            {
                FullName = user.FullName,
            };
        }
    }
}

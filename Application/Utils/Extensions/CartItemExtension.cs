﻿using Application.DTOModel;
using Application.ViewModels;
using Domain.Entities;

namespace Application.Utils.Extensions
{
    public static class CartItemExtension
    {
        public static DTOCartItem ToDTOCartItem(this CartItem cartItem)
        {
            return new DTOCartItem
            {
                BookId = cartItem.BookId,
                UserId = cartItem.UserId,
                Quantity = cartItem.Quantity,
            };
        }

        public static CartItem ToCartItem(this DTOCartItem dtoCartItem)
        {
            return new CartItem
            {
                BookId = dtoCartItem.BookId,
                UserId = dtoCartItem.UserId,
                Quantity = dtoCartItem.Quantity,
                SubtotalPrice = dtoCartItem.SubtotalPrice,
            };
        }

        public static CartItemVM ToCartItemViewModel(this CartItem cartItem)
        {
            return new CartItemVM
            {
                Quantity = cartItem.Quantity,
                SubtotalPrice = cartItem.SubtotalPrice,
            };
        }
    }
}

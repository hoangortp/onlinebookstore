﻿namespace Application.Interfaces
{
    public interface ICurrentTimeService
    {
        public DateTime GetCurrentTime();
    }
}

﻿using Application.DTOModel.DTOUser;
using Application.ViewModels;
using Domain.Entities;

namespace Application.Interfaces.IuserServices
{
    public interface IUserService
    {
        public Task<User> GetUserByEmail(string email);
        public Task<DTOUserUpdate?> UpdateUser(DTOUserUpdate dtoUserUpdate);
        public Task ChargeMoney(DTOUserChargeMoney chargeMoney);
        public AccountBalanceVM ViewAccountBalance(Guid userId);
    }
}

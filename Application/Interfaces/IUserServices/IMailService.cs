﻿namespace Application.Interfaces.IuserServices
{
    public interface IMailService
    {
        public Task SendOTPMailAsync(string email);
        public Task SendReportAsync(string email, string path);
    }
}

﻿using Application.DTOModel.DTOUser;
using Application.DTOModel.DTOUserModels;

namespace Application.Interfaces.IuserServices
{
    public interface IAuthService
    {
        public Task<DTOUserRegister?> RegisterAsync(DTOUserRegister dtoUserRegister);
        public Task<DTOAdminRegister?> RegisterAdminAsync(DTOAdminRegister dtoAdminRegister);
        public Task<string> LoginAsync(DTOUserLogin dtoUserLogin);
        public Task<DTOUserResetPassword?> ResetPassword(DTOUserResetPassword dtoUserResetPassword);
    }
}

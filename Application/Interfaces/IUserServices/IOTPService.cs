﻿namespace Application.Interfaces.IuserServices
{
    public interface IOTPService
    {
        public Task<bool> VerifyOTP(string email, int enteredOTP, ICurrentTimeService currentTimeService);
    }
}

﻿namespace Application.Interfaces
{
    public interface IDailyReportService
    {
        public Task GenerateDailyReport();
    }
}

﻿using Application.DTOModel;
using Domain.Commons;
using Domain.Entities;

namespace Application.Interfaces
{
    public interface IOrderService
    {
        public Task<Order?> AddOrder(Guid userId, DTOOrder dtoOrder);
        public Task<Pagination<DTOOrder>> ViewOrders(Guid userId, int pageNumber, int pageSize);
    }
}

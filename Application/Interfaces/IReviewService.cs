﻿using Application.DTOModel;
using Application.ViewModels;
using Domain.Commons;

namespace Application.Interfaces
{
    public interface IReviewService
    {
        public Task<DTOReview?> AddReview(Guid userId, DTOReview dtoReview);
        public Task<Pagination<ReviewVM>> ViewReviews(Guid bookId, int pageNumber, int pageSize);
    }
}

﻿using Application.ViewModels;
using Domain.Commons;
using Domain.Entities;

namespace Application.Interfaces
{
    public interface IOrderDetailService
    {
        public Task AddOrderDetail(Guid userId, Order order);
        public Task<Pagination<OrderDetailVM>> ViewOrderDetails(Guid orderId, int pageNumber, int pageSize);
    }
}

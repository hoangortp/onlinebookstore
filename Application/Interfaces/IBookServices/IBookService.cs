﻿using Application.DTOModel.DTOBookModels;
using Domain.Commons;

namespace Application.Interfaces.IBookServices
{
    public interface IBookService
    {
        public DTOBook GetBookById(Guid bookId);
        public Task<Pagination<DTOBook>> GetBooks(int pageNumber, int pageSize);
        public Task<DTOBook?> AddBook(DTOCreatedBook dtoCreateBook);
        public Task<List<DTOBook>?> AddRangeBook(List<DTOCreatedBook> dtoCreateBooks);
        public Task<DTOBook?> SoftDeleteBook(Guid bookId);
        public Task<List<DTOBook>?> SoftDeleteRangeBook(List<Guid> id);
        public Task<DTOBook?> UpdateBook(DTOBook dtoBook);
    }
}

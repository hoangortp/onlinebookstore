﻿using Application.DTOModel.DTOBookModels;
using Domain.Commons;

namespace Application.Interfaces.IBookServices
{
    public interface IFilterBookService
    {
        public Task<Pagination<DTOBook>> FilterByTitleAsync(string bookTitle, int pageNumber, int pageSize);
        public Task<Pagination<DTOBook>> FilterByAuthorAsync(string author, int pageNumber, int pageSize);
        public Task<Pagination<DTOBook>> FilterByYearAsync(int year, int pageNumber, int pageSize);
        public Task<Pagination<DTOBook>> FilterByGenreAsync(string genre, int pageNumber, int pageSize);
    }
}

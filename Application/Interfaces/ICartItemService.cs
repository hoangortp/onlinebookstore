﻿using Application.DTOModel;
using Application.ViewModels;
using Domain.Commons;

namespace Application.Interfaces
{
    public interface ICartItemService
    {
        public Task<DTOCartItem?> AddToCart(DTOCartItem dtoCartItem);
        public Task<Pagination<CartItemVM>> ViewCartItems(Guid id, int pageNumber, int pageSize);
        public Task<DTOCartItem?> RemoveFromCart(DTOCartItem cartItem);
    }
}

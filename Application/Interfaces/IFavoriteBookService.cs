﻿using Application.DTOModel;
using Application.ViewModels;
using Domain.Commons;

namespace Application.Interfaces
{
    public interface IFavoriteBookService
    {
        public Task<DTOFavoriteBook?> AddFavoriteBook(DTOFavoriteBook dtoFavoriteBook);
        public Task<Pagination<FavoriteBookVM>> ViewFavoriteBooks(Guid userId, int pageNumber, int pageSize);
        public Task<DTOFavoriteBook?> RemoveFavoriteBook(DTOFavoriteBook dtoFavoriteBook);
    }
}

﻿using Application.Interfaces;
using Application.Interfaces.IBookServices;
using Application.Interfaces.IuserServices;
using Application.Services;
using Application.Services.BookServices;
using Application.Services.CartItemServices;
using Application.Services.UserServices;
using Microsoft.Extensions.DependencyInjection;

namespace Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplicationServices(this IServiceCollection services)
    {
        services.AddScoped<IBookService, BookService>();
        services.AddScoped<IFilterBookService, FilterBookService>();
        services.AddScoped<IOTPService, OTPService>();
        services.AddScoped<IUserService, UserService>();
        services.AddScoped<IAuthService, AuthService>();
        services.AddScoped<IMailService, MailService>();
        services.AddScoped<IFavoriteBookService, FavoriteBookService>();
        services.AddScoped<ICartItemService, CartItemService>();
        services.AddScoped<IOrderService, OrderService>();
        services.AddScoped<IOrderDetailService, OrderDetailService>();
        services.AddScoped<IReviewService, ReviewService>();
        services.AddScoped<IDailyReportService, DailyReportService>();

        return services;
    }
}
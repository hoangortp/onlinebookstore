﻿using Domain.Commons;
using Domain.Entities;

namespace Domain.Repositories
{
    public interface IReviewRepository : IGenericRepository<Review>
    {
        public Task<Pagination<Review>> Paging(Guid bookId, int pageNumber, int pageSize);
    }
}

﻿using Domain.Entities;

namespace Domain.Repositories
{
    public interface ICartItemRepository : IGenericRepository<CartItem>
    {
        public Task<CartItem> GetCartItemByUserIdAndBookId(Guid userId, Guid bookId);
        public List<CartItem> ViewCartItems(Guid userId);
    }
}

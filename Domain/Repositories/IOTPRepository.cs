﻿using Domain.Entities;

namespace Domain.Repositories
{
    public interface IOTPRepository : IGenericRepository<OTP>
    {
        public Task<OTP> GetOTPByEmailAndOTPCode(string email, int otpCode);
    }
}

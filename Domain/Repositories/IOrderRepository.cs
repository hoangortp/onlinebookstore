﻿using Domain.Commons;
using Domain.Entities;

namespace Domain.Repositories
{
    public interface IOrderRepository : IGenericRepository<Order>
    {
        public Task<Pagination<Order>> Paging(Guid userId, int pageNumber, int pageSize);
        public Task<int> CountTodayOrder();
        public Task<int> CountTodayPaidOrder();
        public Task<decimal> GetTotalIncome();
    }
}

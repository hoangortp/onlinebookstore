﻿using Domain.Entities;

namespace Domain.Repositories
{
    public interface IUserRepository : IGenericRepository<User>
    {
        public Task<bool> CheckAdminExists();
        public Task<bool> CheckEmailExisted(string email);
        public Task<User> GetEmailAndHashedPassword(string email, string hashedPassword);
        public Task<string> GetRoleBasedOnEmail(string email);
        public Task<User> GetUserBasedOnEmail(string email);
        public Task<int> CountNewAccount();
    }
}

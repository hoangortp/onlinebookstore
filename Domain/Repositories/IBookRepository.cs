﻿using Domain.Entities;

namespace Domain.Repositories
{
    public interface IBookRepository : IGenericRepository<Book>
    {
        public string GetBookTitleById(Guid bookId);
        public Guid GetIdByBookTitle(string bookTitle);
    }
}

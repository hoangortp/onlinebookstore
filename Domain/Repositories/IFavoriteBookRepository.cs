﻿using Domain.Entities;

namespace Domain.Repositories
{
    public interface IFavoriteBookRepository : IGenericRepository<FavoriteBook>
    {
        public Task<FavoriteBook> GetFavoriteBookByUserIdAndBookId(Guid userId, Guid bookId);
    }
}

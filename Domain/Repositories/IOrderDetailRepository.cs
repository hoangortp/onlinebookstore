﻿using Domain.Commons;
using Domain.Entities;

namespace Domain.Repositories
{
    public interface IOrderDetailRepository : IGenericRepository<OrderDetail>
    {
        public Task<Pagination<OrderDetail>> Paging(Guid orderId, int pageNumber, int pageSize);
    }
}

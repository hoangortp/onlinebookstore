﻿using System.Linq.Expressions;
using Domain.Commons;

namespace Domain.Repositories
{
    public interface IGenericRepository<T>
    {
        Task<ICollection<T>> GetAllAsync();
        T GetById(Guid id);
        Task AddAsync(T entity);
        Task AddRangeAsync(ICollection<T> entities);
        void Update(T entity);
        void UpdateRange(ICollection<T> entities);
        void SoftDelete(T entity);
        void SoftDeleteRange(ICollection<T> entities);
        void HardDelete(T entity);
        void HardDeleteRange(ICollection<T> entities);
        public Task<Pagination<T>> Paging(int pageNumber, int pageSize);
        public Task<Pagination<T>> Filter(Expression<Func<T, bool>> predicate, int pageNumber, int pageSize);
    }
}

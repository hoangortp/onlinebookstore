﻿using Domain.Constants;

namespace Domain.Entities
{
    public class User : BaseEntity
    {
        public string? FullName { get; set; }

        public string? Role { get; set; }

        public string? Email { get; set; }

        public string? HashedPassword { get; set; }

        public string? Salt { get; set; }

        public decimal AccountBalance { get; set; }

        public ICollection<Order>? Orders { get; set; }

        public ICollection<CartItem>? CartItems { get; set; }

        public ICollection<FavoriteBook>? FavoriteBooks { get; set; }
    }
}

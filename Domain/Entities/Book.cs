﻿using Domain.Constants;

namespace Domain.Entities
{
    public class Book : BaseEntity
    {
        public string? Title { get; set; }

        public string? Author { get; set; }

        public string? Genre { get; set; }
        
        public int PublicationYear { get; set; }

        public int StockQuantity { get; set; }

        public decimal Price { get; set; }

        public ICollection<OrderDetail>? OrderDetails { get; set; }

        public ICollection<Review>? Reviews { get; set; }
    }
}

﻿namespace Domain.Entities
{
    public class OTP : BaseEntity
    {
        public string? Email { get; set; }

        public int OTPCode { get; set; }

        public int ExpiresInMinutes => 1;
    }
}

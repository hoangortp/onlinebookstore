﻿using Domain.Constants;

namespace Domain.Entities
{
    public class CartItem : BaseEntity
    {
        public Guid UserId { get; set; }

        public User? User { get; set; }

        public Guid BookId { get; set; }

        public Book? Book { get; set; }

        public int Quantity { get; set; }

        public decimal SubtotalPrice { get; set; }
    }
}

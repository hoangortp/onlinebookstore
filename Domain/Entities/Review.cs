﻿using Domain.Constants;

namespace Domain.Entities
{
    public class Review : BaseEntity
    {
        public Guid BookId { get; set; }

        public Book? Book { get; set; }

        public Guid UserId { get; set; }

        public string? Image { get; set; }

        public string? Content { get; set; }

        public int Rating { get; set; }
    }
}

﻿namespace Domain.Entities
{
    public class OrderDetail : BaseEntity
    {
        public Guid OrderId { get; set; }

        public Guid BookId { get; set; }

        public int Quantity { get; set; }

        public decimal SubtotalPrice { get; set; }

        public Order? Order { get; set; }

        public Book? Book { get; set; }
    }
}

﻿namespace Domain.Entities
{
    public class Order : BaseEntity
    {
        public Guid UserId { get; set; }

        public DateTime PaidDate { get; set; }

        public decimal TotalPrice { get; set; }

        public User? User { get; set; }

        public ICollection<OrderDetail>? OrderDetails { get; set; }
    }
}
